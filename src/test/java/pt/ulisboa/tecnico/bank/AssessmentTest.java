package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class AssessmentTest {
	private static final String NIF_ONE = "123456789";
	private static final String NIF_TWO = "123456788";
	private static final String NIF_THREE = "123456787";
	private static final String NIF_FOUR = "123456786";
	private static final String PHONE_ONE = "987654321";

	@Before
	public void setUp() {
		CheckingAccount.counter = 0;
		SavingsAccount.counter = 0;
		SalaryAccount.counter = 0;
	}

	@Test
	public void assessment() throws ClientException, BankException, AccountException, RemoteAccessException {
		// PART ONE
		Bank bank = new Bank();

		Client client = bank.createClient("Mário", "Gonçalves", 34, AssessmentTest.NIF_ONE, AssessmentTest.PHONE_ONE,
				"Fifth Avenue");

		Account checkingAccount = bank.createAccount(AccountType.CHECKING, client, 100);

		checkingAccount.deposit(1000);
		try {
			checkingAccount.withdraw(1100);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(1100, e.getValue());
			Assert.assertEquals(1000, checkingAccount.getBalance());
		}

		Assert.assertEquals(client, bank.getClientByNIF(AssessmentTest.NIF_ONE));

		try {
			bank.createClient("Mário", "Gonçalves", 34, AssessmentTest.NIF_ONE, AssessmentTest.PHONE_ONE,
					"Fifth Avenue");
			Assert.fail();
		} catch (BankException e) {
			Assert.assertEquals(AssessmentTest.NIF_ONE, e.getValue());
			Assert.assertEquals(1, bank.getNumberOfClients());
		}

		try {
			bank.createClient("Mário", "Gonçalves", 34, "12345", AssessmentTest.PHONE_ONE, "Fifth Avenue");
			Assert.fail();
		} catch (BankException e) {
			Assert.assertEquals("12345", e.getValue());
			Assert.assertEquals(1, bank.getNumberOfClients());
		}

		try {
			bank.createClient("Mário", "Gonçalves", 34, "12345678A", AssessmentTest.PHONE_ONE, "Fifth Avenue");
			Assert.fail();
		} catch (BankException e) {
			Assert.assertEquals("12345678A", e.getValue());
			Assert.assertEquals(1, bank.getNumberOfClients());
		}

		try {
			bank.createClient("Mário", "Gonçalves", 34, "213456789", "1234", "Fifth Avenue");
			Assert.fail();
		} catch (BankException e) {
			Assert.assertEquals("1234", e.getValue());
			Assert.assertEquals(1, bank.getNumberOfClients());
		}

		try {
			bank.createClient("Mário", "Gonçalves", 34, "213456789", "a12345678", "Fifth Avenue");
			Assert.fail();
		} catch (BankException e) {
			Assert.assertEquals("a12345678", e.getValue());
			Assert.assertEquals(1, bank.getNumberOfClients());
		}

		client = bank.createClient("Mário", "Gonçalves", 34, AssessmentTest.NIF_TWO, AssessmentTest.PHONE_ONE,
				"Fifth Avenue");
		Assert.assertEquals(2, bank.getNumberOfClients());

		// PART TWO
		Assert.assertEquals(bank.getClientByNIF(AssessmentTest.NIF_ONE), checkingAccount.getClient());

		Account savingsAccount = bank.createAccount(AccountType.SAVINGS, client, 100);

		client = bank.createClient("Mário", "Gonçalves", 34, AssessmentTest.NIF_THREE, AssessmentTest.PHONE_ONE,
				"Fifth Avenue");
		Account salaryAccount = bank.createAccount(AccountType.SAVINGS, client, 1500);

		Assert.assertEquals(3, bank.getNumberOfAccounts());
		Assert.assertEquals(bank.getClientByNIF(AssessmentTest.NIF_TWO), savingsAccount.getClient());
		Assert.assertEquals(bank.getClientByNIF(AssessmentTest.NIF_THREE), salaryAccount.getClient());

		// PART THREE
		Account accountOne = bank.createAccount(AccountType.SAVINGS, client, 500);
		Account accountTwo = bank.createAccount(AccountType.CHECKING, client, 0);
		Account accountThree = bank.createAccount(AccountType.SALARY, client, 1000);

		Assert.assertEquals(client, accountOne.getClient());
		Assert.assertEquals(client, accountTwo.getClient());
		Assert.assertEquals(client, accountThree.getClient());

		accountOne.deposit(1500);
		accountTwo.deposit(2000);
		accountThree.deposit(500);

		Assert.assertTrue(client.hasAccount(salaryAccount));
		Assert.assertTrue(client.hasAccount(accountOne));
		Assert.assertTrue(client.hasAccount(accountTwo));
		Assert.assertTrue(client.hasAccount(accountThree));
		Assert.assertEquals(4000, client.getTotalBalance());

		Assert.assertEquals("SV3", accountOne.getAccountId());
		Assert.assertEquals("CK2", accountTwo.getAccountId());
		Assert.assertEquals("SL1", accountThree.getAccountId());

		Assert.assertEquals(bank, client.getBank());

		Assert.assertEquals(3, bank.getNumberOfClients());
		try {
			new Client(bank, "Mário", "Gonçalves", 34, AssessmentTest.NIF_THREE, AssessmentTest.PHONE_ONE,
					"Fifth Avenue");
			Assert.fail();
		} catch (ClientException e) {
			Assert.assertEquals(AssessmentTest.NIF_THREE, e.getValue());
			Assert.assertEquals(3, bank.getNumberOfClients());
		}

		// PART FOUR
		savingsAccount.makeInactive();
		Assert.assertFalse(savingsAccount.isActive());

		try {
			savingsAccount.deposit(1000);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(1000, e.getValue());
		}

		try {
			savingsAccount.withdraw(savingsAccount.getBalance());
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(savingsAccount.getBalance(), e.getValue());
		}

		salaryAccount.makeInactive();
		Assert.assertFalse(salaryAccount.isActive());

		try {
			salaryAccount.deposit(1000);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(1000, e.getValue());
		}

		try {
			salaryAccount.withdraw(salaryAccount.getBalance());
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(salaryAccount.getBalance(), e.getValue());
		}

		Assert.assertFalse(bank.getClientByNIF(AssessmentTest.NIF_TWO).isActive());
		Assert.assertTrue(bank.getClientByNIF(AssessmentTest.NIF_THREE).isActive());
		Assert.assertEquals(4, bank.getClientByNIF(AssessmentTest.NIF_THREE).getNumberOfAccounts());

		try {
			bank.createAccount(AccountType.YOUNG, client, 0);
		} catch (AccountException e) {
			Assert.assertEquals(34, e.getValue());
			Assert.assertEquals(4, client.getNumberOfAccounts());
			Assert.assertEquals(6, bank.getNumberOfAccounts());
		}

		Client youngClient = bank.createClient("Mário", "Gonçalves", 17, AssessmentTest.NIF_FOUR,
				AssessmentTest.PHONE_ONE, "Fifth Avenue");

		Account youngAccount = bank.createAccount(AccountType.YOUNG, youngClient, 0);

		youngAccount.deposit(10000);

		try {
			youngAccount.deposit(5);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(5, e.getValue());
			Assert.assertEquals(10000, youngAccount.getBalance());
		}

		try {
			youngAccount.withdraw(1000);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(1000, e.getValue());
			Assert.assertEquals(10000, youngAccount.getBalance());
		}

		youngClient.happyBirthday();

		Assert.assertEquals(0, youngAccount.getBalance());
		Assert.assertFalse(youngAccount.isActive());
		Assert.assertEquals(10002, youngClient.getTotalBalance());
		Assert.assertTrue(youngClient.isActive());
		Assert.assertEquals(2, youngClient.getNumberOfAccounts());
		Assert.assertEquals(8, bank.getNumberOfAccounts());
	}

	@After
	public void tearDown() {
		CheckingAccount.counter = 0;
		SavingsAccount.counter = 0;
		SalaryAccount.counter = 0;
	}

}
