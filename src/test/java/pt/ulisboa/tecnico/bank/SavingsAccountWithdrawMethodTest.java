package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class SavingsAccountWithdrawMethodTest {
	private SavingsAccount account = null;

	@Before
	public void setUp() throws ClientException, AccountException, BankException, RemoteAccessException {
		Client client = new Client(new Bank(), "Mário", "Silva", 34, "117999111", "967777719", "Rua Alves Redol 9");

		this.account = new SavingsAccount(client, 1000, 100);
	}

	@Test
	public void withdrawSuccess() throws AccountException {
		this.account.withdraw(1000);
		Assert.assertEquals(0, this.account.getBalance());
	}

	@Test
	public void withdrawFail() {
		try {
			this.account.withdraw(100);
		} catch (AccountException e) {
			Assert.assertEquals(100, e.getValue());
			Assert.assertEquals(1000, this.account.getBalance());
		}
	}

	@Test
	public void withdrawInInactiveAccount() throws AccountException {
		this.account.withdraw(1000);
		this.account.makeInactive();

		try {
			this.account.withdraw(1000);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(1000, e.getValue());
			Assert.assertEquals(0, this.account.getBalance());
		}
	}

	@After
	public void tearDown() {
		this.account = null;
	}

}
