package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class BankCreateClientMethodTest {
	private static final String FIRST_NAME = "Mário";
	private static final String LAST_NAME = "Silva";
	private static final int AGE = 56;
	private static final String NIF = "117999111";
	private static final String PHONE_NUMBER = "967777719";
	private static final String ADDRESS = "Rua Alves Redol 9";

	private Bank bank;

	@Before
	public void setUp() {
		this.bank = new Bank();
	}

	@Test
	public void success() throws BankException, RemoteAccessException {
		this.bank.createClient(BankCreateClientMethodTest.FIRST_NAME, BankCreateClientMethodTest.LAST_NAME,
				BankCreateClientMethodTest.AGE, BankCreateClientMethodTest.NIF, BankCreateClientMethodTest.PHONE_NUMBER,
				BankCreateClientMethodTest.ADDRESS);

		Client client = this.bank.getClientByNIF(BankCreateClientMethodTest.NIF);
		Assert.assertNotNull(client);
		Assert.assertEquals(1, this.bank.getNumberOfClients());
	}

	@Test
	public void failExistsWithNIF() throws BankException, RemoteAccessException {
		this.bank.createClient(BankCreateClientMethodTest.FIRST_NAME, BankCreateClientMethodTest.LAST_NAME,
				BankCreateClientMethodTest.AGE, BankCreateClientMethodTest.NIF, BankCreateClientMethodTest.PHONE_NUMBER,
				BankCreateClientMethodTest.ADDRESS);

		try {
			this.bank.createClient(BankCreateClientMethodTest.FIRST_NAME, BankCreateClientMethodTest.LAST_NAME,
					BankCreateClientMethodTest.AGE, BankCreateClientMethodTest.NIF,
					BankCreateClientMethodTest.PHONE_NUMBER, BankCreateClientMethodTest.ADDRESS);
			Assert.fail();
		} catch (BankException e) {
			Assert.assertEquals(1, this.bank.getNumberOfClients());

		}

	}

	@After
	public void tearDown() {
		this.bank = null;
	}
}
