package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class AccountCreateMethodTest {

	private Bank bank;
	private Client client;
	private Client youngClient;

	@Before
	public void setUp() throws BankException, RemoteAccessException {
		this.bank = new Bank();
		this.client = this.bank.createClient("Mário", "Silva", 43, "123456789", "987654321", "Avenue 5");
		this.youngClient = this.bank.createClient("Mário", "Silva", 12, "123456788", "987654321", "Avenue 5");
	}

	@Test
	public void createCheckingAccount() throws AccountException, BankException, RemoteAccessException {
		CheckingAccount checkingAccount = new CheckingAccount(this.client, 0);

		Assert.assertTrue(checkingAccount.isActive());
	}

	@Test
	public void createCheckingAccountFailAge() throws AccountException, BankException, RemoteAccessException {
		try {
			new CheckingAccount(this.youngClient, 0);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(12, e.getValue());
			Assert.assertEquals(0, this.bank.getNumberOfAccounts());
		}

	}

	@Test
	public void createSavingsAccount() throws AccountException, BankException, RemoteAccessException {
		SavingsAccount savingsAccount = new SavingsAccount(this.client, 0, 100);

		Assert.assertTrue(savingsAccount.isActive());
	}

	@Test
	public void createSavingsAccountFailAge() throws AccountException, BankException, RemoteAccessException {
		try {
			new SavingsAccount(this.youngClient, 1000, 100);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(12, e.getValue());
			Assert.assertEquals(0, this.bank.getNumberOfAccounts());
		}

	}

	@Test
	public void createSalaryAccount() throws AccountException, BankException, RemoteAccessException {
		SalaryAccount salaryAccount = new SalaryAccount(this.client, 0, 1000);

		Assert.assertTrue(salaryAccount.isActive());
	}

	@Test
	public void createSalaryAccountFailAge() throws AccountException, BankException, RemoteAccessException {
		try {
			new SalaryAccount(this.youngClient, 100, 1000);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(12, e.getValue());
			Assert.assertEquals(0, this.bank.getNumberOfAccounts());
		}

	}

	@Test
	public void createSecondSalaryAccount() throws AccountException, BankException, RemoteAccessException {
		new SalaryAccount(this.client, 100, 1000);
		try {
			new SalaryAccount(this.client, 100, 1000);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(1, e.getValue());
			Assert.assertEquals(1, this.client.getNumberOfAccounts());
		}

	}

	@After
	public void tearDown() {
		this.bank = null;
	}

}
