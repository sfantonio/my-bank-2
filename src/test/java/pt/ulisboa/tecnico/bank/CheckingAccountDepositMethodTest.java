package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class CheckingAccountDepositMethodTest {
	private Account account;

	@Before
	public void setUp() throws ClientException, AccountException, BankException, RemoteAccessException {
		Client client = new Client(new Bank(), "Mário", "Silva", 34, "117999111", "967777719", "Rua Alves Redol 9");

		this.account = new CheckingAccount(client, 500);
	}

	@Test
	public void testPassSingleDeposit() throws AccountException {
		this.account.deposit(600);
		Assert.assertEquals(1100, this.account.getBalance());
	}

	@Test
	public void testPassTwoDeposit() throws AccountException {
		this.account.deposit(600);
		this.account.deposit(500);
		Assert.assertEquals(1600, this.account.getBalance());
	}

	@Test()
	public void testFailDepositNegitive() {
		try {
			this.account.deposit(-100);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(-100, e.getValue());
			Assert.assertEquals(500, this.account.getBalance());
		}
	}

	@Test
	public void depositInInactiveAccount() throws AccountException {
		this.account.withdraw(500);
		this.account.makeInactive();

		try {
			this.account.deposit(500);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(500, e.getValue());
			Assert.assertEquals(0, this.account.getBalance());
		}
	}

	@After
	public void tearDown() {
		this.account = null;
	}

}
