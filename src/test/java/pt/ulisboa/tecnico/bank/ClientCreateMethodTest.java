package pt.ulisboa.tecnico.bank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class ClientCreateMethodTest {
	private static final String FIRST_NAME = "Mário";
	private static final String LAST_NAME = "Silva";
	private static final int AGE = 55;
	private static final String NIF = "117999111";
	private static final String PHONE_NUMBER = "967777719";
	private static final String ADDRESS = "Rua Alves Redol 9";
	private static final String PNONE_NUMBER_ERROR = "12345678";
	private static final String NIF_ERROR = "12345678U";

	private Bank bank;

	@Before
	public void setUp() {
		this.bank = new Bank();
	}

	@Test
	public void success() throws ClientException, BankException, RemoteAccessException {
		Client client = new Client(this.bank, ClientCreateMethodTest.FIRST_NAME, ClientCreateMethodTest.LAST_NAME,
				ClientCreateMethodTest.AGE, ClientCreateMethodTest.NIF, ClientCreateMethodTest.PHONE_NUMBER,
				ClientCreateMethodTest.ADDRESS);

		Assert.assertEquals(this.bank, client.getBank());
		Assert.assertEquals(ClientCreateMethodTest.FIRST_NAME, client.getFirstName());
		Assert.assertEquals(ClientCreateMethodTest.LAST_NAME, client.getLastName());
		Assert.assertEquals(ClientCreateMethodTest.AGE, client.getAge());
		Assert.assertEquals(ClientCreateMethodTest.NIF, client.getNIF());
		Assert.assertEquals(ClientCreateMethodTest.PHONE_NUMBER, client.getPhoneNumber());
		Assert.assertEquals(ClientCreateMethodTest.ADDRESS, client.getAddress());
	}

	@Test
	public void failNIFDigits() throws BankException, RemoteAccessException {
		try {
			new Client(this.bank, ClientCreateMethodTest.FIRST_NAME, ClientCreateMethodTest.LAST_NAME,
					ClientCreateMethodTest.AGE, ClientCreateMethodTest.NIF_ERROR, ClientCreateMethodTest.PHONE_NUMBER,
					ClientCreateMethodTest.ADDRESS);
			Assert.fail();
		} catch (ClientException e) {
			Assert.assertEquals(ClientCreateMethodTest.NIF_ERROR, e.getValue());
		}

	}

	@Test
	public void failPhoneNumber() throws BankException, RemoteAccessException {
		try {
			new Client(this.bank, ClientCreateMethodTest.FIRST_NAME, ClientCreateMethodTest.LAST_NAME,
					ClientCreateMethodTest.AGE, ClientCreateMethodTest.NIF, ClientCreateMethodTest.PNONE_NUMBER_ERROR,
					ClientCreateMethodTest.ADDRESS);
			Assert.fail();
		} catch (ClientException e) {
			Assert.assertEquals(ClientCreateMethodTest.PNONE_NUMBER_ERROR, e.getValue());
		}
	}

}
