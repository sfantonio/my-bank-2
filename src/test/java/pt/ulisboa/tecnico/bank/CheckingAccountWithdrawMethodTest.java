package pt.ulisboa.tecnico.bank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class CheckingAccountWithdrawMethodTest {
	Client client;

	@Before
	public void setUp() throws ClientException, BankException, RemoteAccessException {
		this.client = new Client(new Bank(), "Mário", "Silva", 34, "117999111", "967777719", "Rua Alves Redol 9");
	}

	@Test
	public void successWithdraw() throws AccountException, BankException, RemoteAccessException {
		Account account = new CheckingAccount(this.client, 1000);

		account.withdraw(500);

		Assert.assertEquals(500, account.getBalance());
	}

	@Test
	public void negativeAmmountFail() throws AccountException, BankException, RemoteAccessException {
		Account account = new CheckingAccount(this.client, 1000);

		try {
			account.withdraw(-500);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(-500, e.getValue());
			Assert.assertEquals(1000, account.getBalance());
		}
	}

	@Test
	public void withdrawMoreThanBalanceFail() throws AccountException, BankException, RemoteAccessException {
		Account account = new CheckingAccount(this.client, 1000);

		try {
			account.withdraw(2000);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(2000, e.getValue());
			Assert.assertEquals(1000, account.getBalance());
		}
	}

	@Test
	public void withdrawInInactiveAccount() throws AccountException, BankException, RemoteAccessException {
		Account account = new CheckingAccount(this.client, 1000);
		account.withdraw(1000);
		account.makeInactive();

		try {
			account.withdraw(500);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(500, e.getValue());
			Assert.assertEquals(0, account.getBalance());
		}
	}

	@Test
	public void becomeNegativeAfterWithdrawButSumIsPositive()
			throws AccountException, BankException, RemoteAccessException {
		new CheckingAccount(this.client, 500);
		Account account = new CheckingAccount(this.client, 50);
		new SavingsAccount(this.client, 1000, 100);

		account.withdraw(100);

		Assert.assertEquals(-50, account.getBalance());
	}

	@Test
	public void becomeNegativeAfterWithdrawAndSumIsNegative()
			throws AccountException, BankException, RemoteAccessException {
		new CheckingAccount(this.client, 500);
		Account account = new CheckingAccount(this.client, 50);
		new SavingsAccount(this.client, 1000, 100);

		try {
			account.withdraw(600);
			Assert.fail();
		} catch (final AccountException e) {
			Assert.assertEquals(50, account.getBalance());
		}
	}

}
