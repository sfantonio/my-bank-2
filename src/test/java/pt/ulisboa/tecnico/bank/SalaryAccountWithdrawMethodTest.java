package pt.ulisboa.tecnico.bank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class SalaryAccountWithdrawMethodTest {
	Client client;

	@Before
	public void setUp() throws ClientException, BankException, RemoteAccessException {
		this.client = new Client(new Bank(), "Mário", "Silva", 34, "117999111", "967777719", "Rua Alves Redol 9");
	}

	@Test
	public void negativeBalanceBetweenZeroAndMinusSalary()
			throws AccountException, BankException, RemoteAccessException {
		SalaryAccount salaryAccount = new SalaryAccount(this.client, 1000, 1500);

		salaryAccount.withdraw(1500);

		Assert.assertEquals(-500, salaryAccount.getBalance());
	}

	@Test
	public void negativeBalanceSmallerThanMinusSalary() throws AccountException, BankException, RemoteAccessException {
		SalaryAccount salaryAccount = new SalaryAccount(this.client, 1000, 1500);

		try {
			salaryAccount.withdraw(3000);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(-2000, e.getValue());
			Assert.assertEquals(1000, salaryAccount.getBalance());
		}

	}

	@Test
	public void withdrawInInactiveAccount() throws AccountException, BankException, RemoteAccessException {
		Account account = new SalaryAccount(this.client, 1000, 1000);
		account.withdraw(1000);
		account.makeInactive();

		try {
			account.withdraw(500);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(500, e.getValue());
			Assert.assertEquals(0, account.getBalance());
		}
	}

}
