package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class AccountMakeInactiveMethodTest {
	private Bank bank;
	private Client client;

	@Before
	public void setUp() throws BankException, RemoteAccessException {
		Bank bank = new Bank();
		this.client = bank.createClient("Mário", "Silva", 43, "123456789", "987654321", "Avenue 5");
	}

	@Test
	public void createCheckingAccount() throws AccountException, BankException, RemoteAccessException {
		CheckingAccount account = new CheckingAccount(this.client, 1000);

		try {
			account.makeInactive();
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertTrue(account.isActive());
		}

	}

	@Test
	public void createSavingsAccount() throws AccountException, BankException, RemoteAccessException {
		SavingsAccount account = new SavingsAccount(this.client, 1000, 100);

		try {
			account.makeInactive();
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertTrue(account.isActive());
		}
	}

	@Test
	public void createSalaryAccount() throws AccountException, BankException, RemoteAccessException {
		SalaryAccount account = new SalaryAccount(this.client, 1000, 1000);

		try {
			account.makeInactive();
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertTrue(account.isActive());
		}
	}

	@After
	public void tearDown() {
		this.bank = null;
	}

}
