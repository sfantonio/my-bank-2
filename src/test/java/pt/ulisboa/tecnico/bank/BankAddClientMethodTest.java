package pt.ulisboa.tecnico.bank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class BankAddClientMethodTest {
	private Bank bankOne;

	@Before
	public void setUp() throws BankException {
		this.bankOne = new Bank();
	}

	@Test
	public void addClientToBankSuccess() throws ClientException, BankException, RemoteAccessException {
		Client client = new Client(this.bankOne, "Mário", "Silva", 43, "123456788", "987654321", "Avenue 5");

		this.bankOne.addClient(client);

		Assert.assertNotNull(this.bankOne.getClientByNIF("123456788"));
	}

	@Test
	public void addClientToBankFailNotSameBank() throws ClientException, BankException, RemoteAccessException {
		Client client = new Client(new Bank(), "Mário", "Silva", 43, "123456788", "987654321", "Avenue 5");
		try {
			this.bankOne.addClient(client);
			Assert.fail();
		} catch (BankException e) {
			Assert.assertNull(this.bankOne.getClientByNIF("123456788"));
		}
	}

}
