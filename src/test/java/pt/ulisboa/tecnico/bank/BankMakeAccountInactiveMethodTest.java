package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class BankMakeAccountInactiveMethodTest {
	private static final String FIRST_NAME_ONE = "Mário";
	private static final String LAST_NAME_ONE = "Silva";
	private static final String FIRST_NAME_TWO = "João";
	private static final String LAST_NAME_TWO = "Santos";
	private static final String NIF = "117999111";
	private static final String PHONE_NUMBER = "967777719";
	private static final String ADDRESS = "Rua Alves Redol 9";

	private Bank bank = null;
	private Client client = null;
	private Account account = null;

	@Before
	public void setUp() throws BankException, AccountException, ClientException, RemoteAccessException {
		this.bank = new Bank();
		this.client = new Client(this.bank, "Mário", "Silva", 34, "117999111", "967777719", "Rua Alves Redol 9");
		this.account = this.bank.createAccount(AccountType.CHECKING, this.client, 0);
	}

	@Test
	public void testSuccessInactive() throws BankException, AccountException, RemoteAccessException {
		this.bank.makeAccountInactive(this.account);

		Assert.assertTrue(!this.account.isActive());
		Assert.assertEquals(1, this.bank.getNumberOfAccounts());
		Assert.assertTrue(this.client.hasAccount(this.account));
	}

	@After
	public void tearDown() {
		this.bank = null;
	}

}
