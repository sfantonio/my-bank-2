package pt.ulisboa.tecnico.bank;

import org.junit.Assert;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class ClientHappyBirthdayMethodTest {

	@Test
	public void success() throws ClientException, BankException, AccountException, RemoteAccessException {
		Bank bank = new Bank();
		Client client = new Client(bank, "Mário", "Silva", 17, "117999111", "967777719", "Rua Alves Redol 9");

		Account youngAccountOne = bank.createAccount(AccountType.YOUNG, client, 0);
		Account youngAccountTwo = bank.createAccount(AccountType.YOUNG, client, 0);

		youngAccountOne.deposit(11000);
		youngAccountTwo.deposit(23000);

		client.happyBirthday();

		Assert.assertEquals(4, bank.getNumberOfAccounts());
		Assert.assertEquals(4, client.getNumberOfAccounts());
		Assert.assertFalse(youngAccountOne.isActive());
		Assert.assertFalse(youngAccountTwo.isActive());
		Assert.assertEquals(0, youngAccountOne.getBalance());
		Assert.assertEquals(0, youngAccountTwo.getBalance());
		Assert.assertEquals(34006, client.getTotalBalance());
	}

}
