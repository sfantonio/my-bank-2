package pt.ulisboa.tecnico.bank;

import org.junit.Assert;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class YoungAccountUpgradeMethodTest {

	@Test
	public void testSuccess() throws AccountException, ClientException, BankException, RemoteAccessException {
		Bank bank = new Bank();
		Client client = new Client(bank, "Mário", "Silva", 17, "117999111", "967777719", "Rua Alves Redol 9");
		Account youngAccount = bank.createAccount(AccountType.YOUNG, client, 1000);
		youngAccount.deposit(20000);

		Assert.assertEquals(1, bank.getNumberOfAccounts());

		client.happyBirthday();

		Assert.assertEquals(0, youngAccount.getBalance());
		Assert.assertFalse(youngAccount.isActive());
		Assert.assertEquals(20004, client.getTotalBalance());
		Assert.assertEquals(2, client.getNumberOfAccounts());
		Assert.assertEquals(2, bank.getNumberOfAccounts());

	}

}
