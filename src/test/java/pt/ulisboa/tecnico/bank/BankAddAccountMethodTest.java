package pt.ulisboa.tecnico.bank;

import org.junit.Assert;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class BankAddAccountMethodTest {

	@Test
	public void success() throws BankException, AccountException, RemoteAccessException {
		Bank bank = new Bank();

		Client client = bank.createClient("Mário", "Gonçalves", 34, "123456789", "987654321", "Fifth Avenue");

		Account checkingAccount = new CheckingAccount(client, 1000);

		bank.addAccount(checkingAccount);

		Assert.assertEquals(1, bank.getNumberOfAccounts());
	}

	@Test
	public void failAccountDoesNotBelongToBank() throws BankException, AccountException, RemoteAccessException {
		Bank bank = new Bank();

		Client client = bank.createClient("Mário", "Gonçalves", 34, "123456789", "987654321", "Fifth Avenue");

		Account checkingAccount = new CheckingAccount(client, 1000);

		Bank otherBank = new Bank();
		try {
			otherBank.addAccount(checkingAccount);
		} catch (BankException e) {
			Assert.assertEquals(0, otherBank.getNumberOfAccounts());
		}

	}

}
