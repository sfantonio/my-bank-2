package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class AccountGetNextAccountIdMethodTest {
	Client client;

	@Before
	public void setUp() throws ClientException, BankException, RemoteAccessException {
		CheckingAccount.counter = 0;
		SavingsAccount.counter = 0;
		SalaryAccount.counter = 0;
		this.client = new Client(new Bank(), "Mário", "Silva", 34, "117999111", "967777719", "Rua Alves Redol 9");
	}

	@Test
	public void testCheckingAccount() throws AccountException, BankException, RemoteAccessException {
		CheckingAccount account = new CheckingAccount(this.client, 1000);

		Assert.assertTrue(account.getAccountId().startsWith(AccountType.CHECKING.getPrefix()));
	}

	@Test
	public void testSavingsAccount() throws AccountException, BankException, RemoteAccessException {
		SavingsAccount account = new SavingsAccount(this.client, 1000, 100);

		Assert.assertTrue(account.getAccountId().startsWith(AccountType.SAVINGS.getPrefix()));
	}

	@Test
	public void testSalaryAccount() throws AccountException, BankException, RemoteAccessException {
		SalaryAccount account = new SalaryAccount(this.client, 1000, 100);

		Assert.assertTrue(account.getAccountId().startsWith(AccountType.SALARY.getPrefix()));
	}

	@Test
	public void testYoungAccount() throws AccountException, ClientException, BankException, RemoteAccessException {

		YoungAccount account = new YoungAccount(
				new Client(new Bank(), "Mário", "Silva", 12, "117999111", "967777719", "Rua Alves Redol 9"), 1000);

		Assert.assertTrue(account.getAccountId().startsWith(AccountType.YOUNG.getPrefix()));
	}

	@Test
	public void testSalarySavingsAccount() throws AccountException, BankException, RemoteAccessException {
		SalaryAccount salaryAccount = new SalaryAccount(this.client, 1000, 100);
		SavingsAccount savingsAccount = new SavingsAccount(this.client, 1000, 100);

		Assert.assertEquals(salaryAccount.getAccountId().substring(2, 3),
				savingsAccount.getAccountId().substring(2, 3));
	}

	@Test
	public void testSalaryCheckingAccount() throws AccountException, BankException, RemoteAccessException {
		SalaryAccount salaryAccount = new SalaryAccount(this.client, 1000, 100);
		CheckingAccount checkingAccount = new CheckingAccount(this.client, 1000);

		Assert.assertEquals(salaryAccount.getAccountId().substring(2, 3),
				checkingAccount.getAccountId().substring(2, 3));
	}

	@Test
	public void testCheckingSavingsAccount() throws AccountException, BankException, RemoteAccessException {
		CheckingAccount checkingAccount = new CheckingAccount(this.client, 1000);
		SavingsAccount savingsAccount = new SavingsAccount(this.client, 1000, 100);

		Assert.assertEquals(checkingAccount.getAccountId().substring(2, 3),
				savingsAccount.getAccountId().substring(2, 3));
	}

	@After
	public void tearDown() {
		CheckingAccount.counter = 0;
		SavingsAccount.counter = 0;
		SalaryAccount.counter = 0;
	}

}
