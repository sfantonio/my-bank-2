package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class BankCreateAccountMethodTest {
	private static final String FIRST_NAME_ONE = "Mário";
	private static final String LAST_NAME_ONE = "Silva";
	private static final int AGE = 43;
	private static final String NIF = "117999111";
	private static final String PHONE_NUMBER = "967777719";
	private static final String ADDRESS = "Rua Alves Redol 9";

	private Bank bank;
	private Client client;

	@Before
	public void setUp() throws BankException, AccountException, ClientException, RemoteAccessException {
		this.bank = new Bank();
		this.client = new Client(this.bank, BankCreateAccountMethodTest.FIRST_NAME_ONE,
				BankCreateAccountMethodTest.LAST_NAME_ONE, BankCreateAccountMethodTest.AGE,
				BankCreateAccountMethodTest.NIF, BankCreateAccountMethodTest.PHONE_NUMBER,
				BankCreateAccountMethodTest.ADDRESS);
		this.bank.createAccount(AccountType.CHECKING, this.client, 0);
	}

	@Test
	public void createCheckingAccount() throws BankException, AccountException, ClientException, RemoteAccessException {
		Account account = this.bank.createAccount(AccountType.CHECKING, this.client, 0);

		Assert.assertEquals(2, this.bank.getNumberOfAccounts());
		Assert.assertEquals(2, this.client.getNumberOfAccounts());
		Assert.assertTrue(this.client.hasAccount(account));
	}

	@Test
	public void createSavingsAccount() throws BankException, AccountException, ClientException, RemoteAccessException {
		Account account = this.bank.createAccount(AccountType.SAVINGS, this.client, 500);

		Assert.assertEquals(2, this.bank.getNumberOfAccounts());
		Assert.assertEquals(2, this.client.getNumberOfAccounts());
		Assert.assertTrue(this.client.hasAccount(account));
	}

	@Test
	public void createSalaryAccount() throws BankException, AccountException, ClientException, RemoteAccessException {
		Account account = this.bank.createAccount(AccountType.SALARY, this.client, 1500);

		Assert.assertEquals(2, this.bank.getNumberOfAccounts());
		Assert.assertEquals(2, this.client.getNumberOfAccounts());
		Assert.assertTrue(this.client.hasAccount(account));
	}

	@Test
	public void createYoungAccount() throws BankException, AccountException, ClientException, RemoteAccessException {
		Client youngClient = new Client(this.bank, "Mário", "Silva", 12, "117999112", "967777719",
				"Rua Alves Redol 9");
		Account account = this.bank.createAccount(AccountType.YOUNG, youngClient, 1500);

		Assert.assertEquals(2, this.bank.getNumberOfAccounts());
		Assert.assertEquals(1, this.client.getNumberOfAccounts());
		Assert.assertFalse(this.client.hasAccount(account));
		Assert.assertEquals(1, youngClient.getNumberOfAccounts());
		Assert.assertTrue(youngClient.hasAccount(account));
	}

	@Test
	public void clientHasSeveralAccounts()
			throws ClientException, AccountException, BankException, RemoteAccessException {
		this.bank.createAccount(AccountType.CHECKING, this.client, 0);
		this.bank.createAccount(AccountType.CHECKING, this.client, 0);
		this.bank.createAccount(AccountType.CHECKING, this.client, 0);

		Assert.assertEquals(4, this.client.getNumberOfAccounts());
	}

	@After
	public void tearDown() {
		this.bank = null;
	}

}
