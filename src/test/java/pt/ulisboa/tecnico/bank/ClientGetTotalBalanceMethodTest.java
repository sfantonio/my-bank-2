package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class ClientGetTotalBalanceMethodTest {
	private Client client;

	@Before
	public void setUp() throws ClientException, BankException, RemoteAccessException {
		this.client = new Client(new Bank(), "Mário", "Silva", 34, "117999111", "967777719", "Rua Alves Redol 9");
	}

	@Test
	public void testSumOfThree() throws AccountException, BankException, RemoteAccessException {
		this.client.addAccount(new CheckingAccount(this.client, 1000));
		this.client.addAccount(new SavingsAccount(this.client, 2000, 100));
		this.client.addAccount(new SalaryAccount(this.client, 2000, 1500));

		Assert.assertEquals(5000, this.client.getTotalBalance());
	}

	@After
	public void tearDown() {
		this.client = null;
	}

}
