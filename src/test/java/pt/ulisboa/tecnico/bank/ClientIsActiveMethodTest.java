package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class ClientIsActiveMethodTest {

	private Bank bank;
	private Client client;

	@Before
	public void setUp() throws BankException, RemoteAccessException {
		Bank bank = new Bank();
		this.client = bank.createClient("Mário", "Silva", 43, "123456789", "987654321", "Avenue 5");
	}

	@Test
	public void inactiveAndActiveAccounts() throws AccountException, BankException, RemoteAccessException {
		CheckingAccount account = new CheckingAccount(this.client, 0);
		account.makeInactive();
		account = new CheckingAccount(this.client, 1000);

		Assert.assertTrue(this.client.isActive());
	}

	@Test
	public void threeInactiveAccounts() throws AccountException, BankException, RemoteAccessException {
		CheckingAccount account = new CheckingAccount(this.client, 0);
		account.makeInactive();
		account = new CheckingAccount(this.client, 0);
		account.makeInactive();
		account = new CheckingAccount(this.client, 0);
		account.makeInactive();

		Assert.assertFalse(this.client.isActive());
	}

	@After
	public void tearDown() {

	}

}