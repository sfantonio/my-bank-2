package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class BankGetTotalBalanceMethodTest {
	private Bank bank;
	private Client client;

	@Before
	public void setUp() throws ClientException, BankException, RemoteAccessException {
		this.bank = new Bank();
		this.client = new Client(this.bank, "Mário", "Silva", 34, "117999111", "967777719", "Rua Alves Redol 9");
	}

	@Test
	public void testSumOfThree() throws AccountException, BankException, RemoteAccessException {
		this.bank.createAccount(AccountType.CHECKING, this.client, 0).deposit(1000);
		this.bank.createAccount(AccountType.SALARY, this.client, 1500).withdraw(250);
		this.bank.createAccount(AccountType.SAVINGS, this.client, 100).deposit(500);

		Assert.assertEquals(1250, this.bank.getTotalBalance());
	}

	@After
	public void tearDown() {
		this.bank = null;
	}

}
