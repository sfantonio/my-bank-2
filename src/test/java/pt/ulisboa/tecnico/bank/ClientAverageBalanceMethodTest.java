package pt.ulisboa.tecnico.bank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class ClientAverageBalanceMethodTest {
	private Bank bankOne;
	private Client clientOne;

	@Before
	public void setUp() throws BankException, RemoteAccessException {
		this.bankOne = new Bank();
		this.clientOne = this.bankOne.createClient("Mário", "Silva", 43, "123456789", "987654321", "Avenue 5");
	}

	@Test
	public void averageThreeActiveAccountsTest() throws AccountException, BankException, RemoteAccessException {
		new CheckingAccount(this.clientOne, 1000);
		new SavingsAccount(this.clientOne, 1500, 100);
		new SalaryAccount(this.clientOne, 700, 1000);

		final float average = this.clientOne.averageBalance();

		Assert.assertEquals(1066.6, average, 0.1);
	}

	@Test
	public void averageTwoInactiveAccountsTest() throws AccountException, BankException, RemoteAccessException {
		new CheckingAccount(this.clientOne, 0).makeInactive();
		new SavingsAccount(this.clientOne, 0, 100).makeInactive();

		final float average = this.clientOne.averageBalance();

		Assert.assertEquals(0, average, 0.00001);
	}

	@Test
	public void averageTwoActiveOneInactiveAccountsTest()
			throws AccountException, BankException, RemoteAccessException {
		new CheckingAccount(this.clientOne, 1000);
		new SavingsAccount(this.clientOne, 1500, 100);
		new SalaryAccount(this.clientOne, 0, 800).makeInactive();

		final float average = this.clientOne.averageBalance();

		Assert.assertEquals(1250, average, 0.00001);
	}

}
