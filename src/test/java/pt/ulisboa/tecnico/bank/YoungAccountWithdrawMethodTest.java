package pt.ulisboa.tecnico.bank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class YoungAccountWithdrawMethodTest {
	private YoungAccount account = null;

	@Before
	public void setUp() throws ClientException, AccountException, BankException, RemoteAccessException {
		Client client = new Client(new Bank(), "Mário", "Silva", 13, "117999111", "967777719", "Rua Alves Redol 9");

		this.account = new YoungAccount(client, 1000);
	}

	@Test
	public void failWithdraw() {
		try {
			this.account.withdraw(1000);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(1000, e.getValue());
			Assert.assertEquals(1000, this.account.getBalance());
		}
	}

}
