package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class CreateYoungAccountMethodTest {
	private Bank bank;

	@Before
	public void setUp() throws ClientException {
		this.bank = new Bank();
	}

	@Test
	public void success() throws ClientException, AccountException, BankException, RemoteAccessException {
		Client client = new Client(this.bank, "Mário", "Silva", 12, "117999111", "967777719", "Rua Alves Redol 9");
		YoungAccount youngAccount = new YoungAccount(client, 1000);

		Assert.assertEquals(10, youngAccount.getBase());
	}

	@Test
	public void notBelow18Client() throws ClientException, BankException, RemoteAccessException {
		Client client = new Client(this.bank, "Mário", "Silva", 43, "117999111", "967777719", "Rua Alves Redol 9");

		try {
			new YoungAccount(client, 1000);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(0, this.bank.getNumberOfAccounts());
			Assert.assertEquals(0, client.getNumberOfAccounts());
		}

	}

	@After
	public void tearDown() {
		this.bank = null;
	}

}
