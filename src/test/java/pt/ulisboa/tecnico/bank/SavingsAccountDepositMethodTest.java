package pt.ulisboa.tecnico.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class SavingsAccountDepositMethodTest {
	SavingsAccount savingsAccount;

	@Before
	public void setUp() throws ClientException, AccountException, BankException, RemoteAccessException {
		Client client = new Client(new Bank(), "Mário", "Silva", 34, "117999111", "967777719", "Rua Alves Redol 9");

		this.savingsAccount = new SavingsAccount(client, 1500, 100);
	}

	@Test
	public void depositSuccess() throws AccountException {
		this.savingsAccount.deposit(1000);
		Assert.assertEquals(2500, this.savingsAccount.getBalance());
	}

	@Test
	public void depositFailNotMultiple() throws AccountException {
		try {
			this.savingsAccount.deposit(1050);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(1050, e.getValue());
			Assert.assertEquals(1500, this.savingsAccount.getBalance());
		}
	}

	@Test
	public void earnPointsTest() throws AccountException {
		this.savingsAccount.deposit(500);

		Assert.assertEquals(5, this.savingsAccount.getPoints());
	}

	@Test
	public void depositInInactiveAccount() throws AccountException {
		this.savingsAccount.withdraw(1500);
		this.savingsAccount.makeInactive();

		try {
			this.savingsAccount.deposit(500);
			Assert.fail();
		} catch (AccountException e) {
			Assert.assertEquals(500, e.getValue());
			Assert.assertEquals(0, this.savingsAccount.getBalance());
		}
	}

	@After
	public void tearDown() {
		this.savingsAccount = null;
	}

}
