package pt.ulisboa.tecnico.bank;

import org.junit.Assert;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class BankGetNumberOfActiveAccountsMethodTest {

	@Test
	public void test() throws BankException, AccountException, RemoteAccessException {
		Bank bank = new Bank();

		Client client = bank.createClient("Mário", "Gonçalves", 34, "123456789", "987654321", "Fifth Avenue");

		Account checkingAccount = bank.createAccount(AccountType.CHECKING, client, 0);
		Account salaryAccount = bank.createAccount(AccountType.SALARY, client, 0);

		checkingAccount.makeInactive();

		int active = bank.getNumberOfActiveAccountsAccounts();

		Assert.assertEquals(1, active);
	}

}
