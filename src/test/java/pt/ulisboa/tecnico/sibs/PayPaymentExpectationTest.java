package pt.ulisboa.tecnico.sibs;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.CheckingAccount;
import pt.ulisboa.tecnico.bank.Client;
import pt.ulisboa.tecnico.bank.ServiceAccount;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.sibs.Operation.State;

public class PayPaymentExpectationTest {
	private Bank bankOne;
	private Bank bankTwo;
	private Client clientOne;
	private Client company;

	@Before
	public void setUp() throws BankException, RemoteAccessException {
		this.bankOne = new Bank();
		this.clientOne = this.bankOne.createClient("Mário", "Silva", 50, "123456788", "987654321", "Avenue 5");
		this.bankTwo = new Bank();
		this.company = this.bankTwo.createClient("Company", "One", 20, "111111111", "987654321", "Avenue 5");
	}

	@Test
	public void failPayDueInvalidRegister()
			throws AccountException, BankException, PaymentExpectationException, RemoteAccessException {
		Account accountOne = new CheckingAccount(this.clientOne, 1000);
		ServiceAccount accountTwo = new ServiceAccount(this.company);
		accountTwo.makeInactive();

		final SIBS sibs = SIBS.getInstance();
		sibs.registerPaymentExpectation(accountTwo, 100);
		sibs.processOperations();
		sibs.payPaymentExpectation(accountOne, "100000001", 100);
		sibs.processOperations();

		Assert.assertEquals(2, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(0).getState());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(1).getState());

		Assert.assertEquals(1000, accountOne.getBalance());
		Assert.assertEquals(0, accountTwo.getBalance());
	}

	@Test
	public void sucessPay() throws AccountException, BankException, PaymentExpectationException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final ServiceAccount accountTwo = new ServiceAccount(this.company);

		final SIBS sibs = SIBS.getInstance();
		sibs.registerPaymentExpectation(accountTwo, 100);
		sibs.processOperations();
		sibs.payPaymentExpectation(accountOne, "100000001", 100);
		sibs.processOperations();

		Assert.assertEquals(2, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(0).getState());
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(1).getState());
		Assert.assertEquals(900, accountOne.getBalance());
		Assert.assertEquals(100, accountTwo.getBalance());
	}

	@Test
	public void failDueAlreadyPaid()
			throws AccountException, BankException, PaymentExpectationException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final ServiceAccount accountTwo = new ServiceAccount(this.company);

		final SIBS sibs = SIBS.getInstance();
		sibs.registerPaymentExpectation(accountTwo, 100);
		sibs.processOperations();
		sibs.payPaymentExpectation(accountOne, "100000001", 100);
		sibs.processOperations();
		sibs.payPaymentExpectation(accountOne, "100000001", 100);
		sibs.processOperations();

		Assert.assertEquals(3, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(0).getState());
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(1).getState());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(2).getState());
		Assert.assertEquals(900, accountOne.getBalance());
		Assert.assertEquals(100, accountTwo.getBalance());
	}

	@Test
	public void failPaidDueInvalidAmount()
			throws AccountException, BankException, PaymentExpectationException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final ServiceAccount accountTwo = new ServiceAccount(this.company);

		final SIBS sibs = SIBS.getInstance();
		sibs.registerPaymentExpectation(accountTwo, 100);
		sibs.processOperations();
		sibs.payPaymentExpectation(accountOne, "100000001", 200);
		sibs.processOperations();

		Assert.assertEquals(2, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(0).getState());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(1).getState());
		Assert.assertEquals(1000, accountOne.getBalance());
		Assert.assertEquals(0, accountTwo.getBalance());

	}

	@After
	public void tearDown() {
		Operation.counter = 0;
		SIBS sibs = SIBS.getInstance();
		sibs.operations.clear();
		// registerPaymentExpectationOperation.getReference() = null;
		bankOne = null;
		bankTwo = null;
		clientOne = null;
		company = null;
		registerPaymentExpectationOperation.setACounter();
	}

}
