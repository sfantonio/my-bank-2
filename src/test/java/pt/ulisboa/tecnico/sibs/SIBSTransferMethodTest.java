package pt.ulisboa.tecnico.sibs;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.CheckingAccount;
import pt.ulisboa.tecnico.bank.Client;
import pt.ulisboa.tecnico.bank.SavingsAccount;
import pt.ulisboa.tecnico.bank.YoungAccount;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;
import pt.ulisboa.tecnico.sibs.Operation.State;

public class SIBSTransferMethodTest {
	private Bank bankOne;
	private Bank bankTwo;
	private Client clientOne;
	private Client clientTwo;

	@Before
	public void setUp() throws BankException, RemoteAccessException {
		this.bankOne = new Bank();
		this.clientOne = this.bankOne.createClient("Mário", "Silva", 43, "123456789", "987654321", "Avenue 5");
		this.bankTwo = new Bank();
		this.clientTwo = this.bankTwo.createClient("Mário", "Silva", 43, "123456789", "987654321", "Avenue 5");
	}

	@Test
	public void sucessTransfer() throws AccountException, BankException, SIBSException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final Account accountTwo = new CheckingAccount(this.clientTwo, 1000);

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 100, this.bankTwo, accountTwo.getAccountId());
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(0).getState());
		Assert.assertEquals(900, accountOne.getBalance());
		Assert.assertEquals(1100, accountTwo.getBalance());
	}

	@Test
	public void severalTransfer() throws AccountException, BankException, SIBSException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final Account accountTwo = new CheckingAccount(this.clientTwo, 1000);

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 100, this.bankTwo, accountTwo.getAccountId());
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 1000, this.bankTwo, accountTwo.getAccountId());
		sibs.processOperations();

		Assert.assertEquals(2, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(0).getState());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(1).getState());
		Assert.assertEquals(900, accountOne.getBalance());
		Assert.assertEquals(1100, accountTwo.getBalance());

		sibs.processOperations();

		Assert.assertEquals(2, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(0).getState());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(1).getState());
		Assert.assertEquals(900, accountOne.getBalance());
		Assert.assertEquals(1100, accountTwo.getBalance());
	}

	@Test
	public void failTransferDueToSameBank() throws AccountException, BankException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final Account accountTwo = new CheckingAccount(this.clientOne, 1000);

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 100, this.bankOne, accountTwo.getAccountId());
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(0).getState());
		Assert.assertEquals(1000, accountOne.getBalance());
		Assert.assertEquals(1000, accountTwo.getBalance());
	}

	@Test
	public void failTransferDueToFromAccountNull() throws AccountException, BankException, RemoteAccessException {

		final Account accountTwo = new CheckingAccount(this.clientTwo, 1000);

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, null, 100, new Bank(), accountTwo.getAccountId());
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(0).getState());

		Assert.assertEquals(1000, accountTwo.getBalance());
	}

	@Test
	public void failTransferDueToAccountDoesNotBelongToBank()
			throws AccountException, BankException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final Account accountTwo = new CheckingAccount(this.clientTwo, 1000);

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 100, new Bank(), accountTwo.getAccountId());
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(0).getState());
		Assert.assertEquals(1000, accountOne.getBalance());
		Assert.assertEquals(1000, accountTwo.getBalance());
	}

	@Test
	public void sucessTransferIsLogged() throws AccountException, BankException, SIBSException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final Account accountTwo = new CheckingAccount(this.clientTwo, 1000);

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 100, this.bankTwo, accountTwo.getAccountId());

		sibs.processOperations();

		TransferOperation loggedOperation = (TransferOperation) sibs.operations.get(sibs.operations.size() - 1);

		Assert.assertEquals(1, sibs.operations.size());
		Assert.assertEquals(State.DONE, loggedOperation.getState());
		Assert.assertEquals(this.bankOne, loggedOperation.getFromBank());
		Assert.assertEquals(accountOne.getAccountId(), loggedOperation.getFromAccountId());
		Assert.assertEquals(100, loggedOperation.getAmount());
		Assert.assertEquals(this.bankTwo, loggedOperation.getToBank());
		Assert.assertEquals(accountTwo.getAccountId(), loggedOperation.getToAccountId());
	}

	@Test
	public void failTransferDueToSameBankNotLogged() throws AccountException, BankException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final Account accountTwo = new CheckingAccount(this.clientTwo, 1000);

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 100, this.bankOne, accountTwo.getAccountId());
		sibs.processOperations();

		Assert.assertEquals(1, sibs.operations.size());
	}

	@Test
	public void failTransferDueToToAccountIsNotActive() throws AccountException, BankException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final Account accountTwo = new CheckingAccount(this.clientTwo, 0);
		accountTwo.makeInactive();

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 100, this.bankTwo, accountTwo.getAccountId());
		sibs.processOperations();

		Assert.assertEquals(1000, accountOne.getBalance());
		Assert.assertEquals(0, accountTwo.getBalance());
		Assert.assertEquals(1, sibs.operations.size());
		Operation loggedOperation = sibs.operations.get(sibs.operations.size() - 1);
		Assert.assertEquals(State.ERROR, loggedOperation.getState());

	}

	@Test
	public void failTransferDueToAmountValueToSavingsAccountIsNotMultiple()
			throws AccountException, BankException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final Account accountTwo = new SavingsAccount(this.clientTwo, 1000, 100);

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 150, this.bankTwo, accountTwo.getAccountId());
		sibs.processOperations();

		Assert.assertEquals(1000, accountOne.getBalance());
		Assert.assertEquals(1000, accountTwo.getBalance());
		Assert.assertEquals(1, sibs.operations.size());
	}

	@Test
	public void failedTransferdueToAmountValueTOYOungAccountNotBaseMultiple()
			throws AccountException, BankException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);

		Client clientThree = this.bankTwo.createClient("Mário", "Silva", 15, "999999999", "987654321", "Avenue 5");
		final Account accountThree = new YoungAccount(clientThree, 1000);

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 5, this.bankTwo, accountThree.getAccountId());
		sibs.processOperations();

		Assert.assertEquals(1000, accountOne.getBalance());
		Assert.assertEquals(1000, accountThree.getBalance());
		Assert.assertEquals(1, sibs.operations.size());
	}

	@Test
	public void createLoggedOperationTest() throws AccountException, BankException {
		TransferOperation loggedOperation = new TransferOperation(this.bankOne, "XXX", 100, this.bankTwo, "YYY");

		Assert.assertEquals(1, loggedOperation.getLogId());

		loggedOperation = new TransferOperation(this.bankOne, "XXX", 100, this.bankTwo, "YYY");

		Assert.assertEquals(2, loggedOperation.getLogId());
	}

	@Test
	public void undoTransferTest() throws AccountException, BankException, SIBSException, RemoteAccessException {
		final Account accountOne = new CheckingAccount(this.clientOne, 1000);
		final Account accountTwo = new CheckingAccount(this.clientTwo, 1000);

		final SIBS sibs = SIBS.getInstance();
		sibs.transfer(this.bankOne, accountOne.getAccountId(), 100, this.bankTwo, accountTwo.getAccountId());
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(0).getState());
		Assert.assertEquals(900, accountOne.getBalance());
		Assert.assertEquals(1100, accountTwo.getBalance());
		SIBS.getInstance().operations.get(0).setState(State.UNDO);
		SIBS.getInstance().processOperations();
		SIBS.getInstance().operations.get(0).setState(State.UNDONE);
		Assert.assertEquals(1000, accountOne.getBalance());
		Assert.assertEquals(1000, accountTwo.getBalance());

	}

	@After
	public void tearDown() {
		Operation.counter = 0;
		SIBS sibs = SIBS.getInstance();
		sibs.operations.clear();

	}

}
