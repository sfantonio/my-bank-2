package pt.ulisboa.tecnico.sibs;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.Client;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.sibs.Operation.State;

public class RequestDirectDebitOperationTest {
	private Bank bankOne;
	private Bank bankTwo;
	private Client clientOne;
	private Client clientTwo;
	private Client clientThree;
	private Account accountOne;
	private Account accountTwo;
	private Account accountThree;
	final SIBS sibs = SIBS.getInstance();
	String authorizationId;

	@Before
	public void setUp() throws BankException, AccountException, RemoteAccessException {
		this.bankOne = new Bank();
		this.bankTwo = new Bank();
		this.clientOne = this.bankOne.createClient("Andre", "Santos", 20, "111111111", "987654321", "Avenue 5");
		this.clientTwo = this.bankOne.createClient("Antonio", "Fernandes", 24, "555555555", "666666666", "Poarto");
		this.clientThree = this.bankTwo.createClient("Antonio", "Fernandes", 24, "777777777", "666666666", "Poarto");
		accountOne = bankOne.createAccount(AccountType.CHECKING, clientOne, 0);
		accountTwo = bankOne.createAccount(AccountType.CHECKING, clientTwo, 0);
		accountThree = bankTwo.createAccount(AccountType.SAVINGS, clientThree, 100);
		accountOne.deposit(1000);
		accountTwo.deposit(1000);

		sibs.RegisterAuthorization(accountOne, accountTwo, 600, 2);
		sibs.processOperations();
		for (Operation op : sibs.getOperations()) {
			if (op instanceof RegisterAuthorizationOperation) {
				authorizationId = ((RegisterAuthorizationOperation) op).getAuthorizationId();
				break;
			}
		}

	}

	@Test
	public void SuccessRequestDirectDebitTest() throws AccountException, BankException {

		Assert.assertEquals(accountOne.getBalance(), 1000);
		Assert.assertEquals(accountTwo.getBalance(), 1000);

		sibs.RequestDirectDebit(authorizationId, 500);
		sibs.processOperations();

		Assert.assertEquals(2, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, sibs.operations.get(1).getState());
		Assert.assertEquals("1", authorizationId);

		Assert.assertEquals(accountOne.getBalance(), 500);
		Assert.assertEquals(accountTwo.getBalance(), 1500);
	}

	@Test
	public void excessOfRequests() throws AccountException, BankException {
		sibs.RequestDirectDebit(authorizationId, 200);
		sibs.processOperations();
		Assert.assertEquals(2, SIBS.getInstance().operations.size());
		sibs.RequestDirectDebit(authorizationId, 200);
		sibs.processOperations();

		Assert.assertEquals(3, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, sibs.operations.get(2).getState());

		sibs.RequestDirectDebit(authorizationId, 100);
		sibs.processOperations();

		Assert.assertEquals(4, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, sibs.operations.get(3).getState());

	}

	@Test
	public void nullAuthorizationIdTest() {
		sibs.RequestDirectDebit(null, 200);
		sibs.processOperations();

		Assert.assertEquals(State.ERROR, sibs.operations.get(1).getState());

	}

	@Test
	public void negativeTransactionTest() {
		sibs.RequestDirectDebit(this.authorizationId, -300);
		sibs.processOperations();

		Assert.assertEquals(State.ERROR, sibs.operations.get(1).getState());
		Assert.assertEquals("1", authorizationId);
	}

	@Test
	public void exceededMaxAmount() {
		sibs.RequestDirectDebit(this.authorizationId, 800);
		sibs.processOperations();

		Assert.assertEquals(State.ERROR, sibs.operations.get(1).getState());
	}

	@Test
	public void invalidIdTest() {
		sibs.RequestDirectDebit("Hi", 800);
		sibs.processOperations();

		Assert.assertEquals(State.ERROR, sibs.operations.get(1).getState());
	}

	@Test
	public void currentRequestTest() {
		Assert.assertEquals(accountOne.getBalance(), 1000);
		Assert.assertEquals(accountTwo.getBalance(), 1000);

		sibs.RequestDirectDebit(authorizationId, 500);
		sibs.processOperations();

	}

	@Test
	public void undoRequestDebitTest() {
		Assert.assertEquals(accountOne.getBalance(), 1000);
		Assert.assertEquals(accountTwo.getBalance(), 1000);

		sibs.RequestDirectDebit(authorizationId, 500);
		sibs.processOperations();

		Assert.assertEquals(State.DONE, sibs.operations.get(1).getState());

		sibs.operations.get(1).setState(State.UNDO);
		sibs.processOperations();

		Assert.assertEquals(State.UNDONE, sibs.operations.get(1).getState());

		Assert.assertEquals(accountOne.getBalance(), 1000);
		Assert.assertEquals(accountTwo.getBalance(), 1000);

	}

	@After
	public void tearDown() {
		Operation.counter = 0;
		RegisterAuthorizationOperation.counter = 0;
		SIBS sibs = SIBS.getInstance();
		sibs.operations.clear();

	}

}
