package pt.ulisboa.tecnico.sibs;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.CheckingAccount;
import pt.ulisboa.tecnico.bank.Client;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.sibs.Operation.State;

public class RegisterAuthorizationTest {

	private Bank bankOne;
	private Client clientOne;
	private Client clientTwo;
	final SIBS sibs = SIBS.getInstance();

	@Before
	public void setUp() throws BankException, RemoteAccessException {
		this.bankOne = new Bank();
		this.clientOne = this.bankOne.createClient("Andre", "Santos", 20, "111111111", "987654321", "Avenue 5");
		this.clientTwo = this.bankOne.createClient("Antonio", "Fernandes", 24, "555555555", "666666666", "Poarto");

	}

	@Test
	public void successRegisterAuthorization() throws AccountException, BankException, RemoteAccessException {
		final CheckingAccount accountOne = new CheckingAccount(this.clientOne);
		final CheckingAccount accountTwo = new CheckingAccount(this.clientTwo);

		accountOne.deposit(1000);
		accountTwo.deposit(1000);

		sibs.RegisterAuthorization(accountOne, accountTwo, 600, 3);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, sibs.operations.get(0).getState());

	}

	@Test
	public void sameClientDebitTest() throws AccountException, BankException, RemoteAccessException {
		final CheckingAccount accountTwo = new CheckingAccount(this.clientTwo);
		final SIBS sibs = SIBS.getInstance();
		accountTwo.deposit(1000);

		sibs.RegisterAuthorization(accountTwo, accountTwo, 600, 3);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, sibs.operations.get(0).getState());

	}

	@Test
	public void accountOneNullTest() throws AccountException, BankException, RemoteAccessException {
		final CheckingAccount accountTwo = new CheckingAccount(this.clientTwo);
		final SIBS sibs = SIBS.getInstance();
		accountTwo.deposit(1000);

		sibs.RegisterAuthorization(null, accountTwo, 600, 3);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, sibs.operations.get(0).getState());

	}

	@Test
	public void accountOTwoNullTest() throws AccountException, BankException, RemoteAccessException {
		final CheckingAccount accountOne = new CheckingAccount(this.clientTwo);
		final SIBS sibs = SIBS.getInstance();
		accountOne.deposit(1000);

		sibs.RegisterAuthorization(accountOne, null, 600, 3);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, sibs.operations.get(0).getState());

	}

	@Test
	public void negativeAmountTest() throws AccountException, BankException, RemoteAccessException {
		final CheckingAccount accountOne = new CheckingAccount(this.clientOne);
		final CheckingAccount accountTwo = new CheckingAccount(this.clientTwo);

		accountOne.deposit(1000);
		accountTwo.deposit(1000);

		sibs.RegisterAuthorization(accountOne, accountTwo, -600, 3);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, sibs.operations.get(0).getState());

	}

	@Test
	public void inactiveAccountOneTest() throws AccountException, BankException, RemoteAccessException {
		final CheckingAccount accountOne = new CheckingAccount(this.clientOne);
		final CheckingAccount accountTwo = new CheckingAccount(this.clientTwo);

		accountTwo.deposit(1000);

		accountOne.makeInactive();

		sibs.RegisterAuthorization(accountOne, accountTwo, 600, 3);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, sibs.operations.get(0).getState());

	}

	@Test
	public void inactiveAccountTwoTest() throws AccountException, BankException, RemoteAccessException {
		final CheckingAccount accountOne = new CheckingAccount(this.clientOne);
		final CheckingAccount accountTwo = new CheckingAccount(this.clientTwo);

		accountOne.deposit(1000);

		accountTwo.makeInactive();

		sibs.RegisterAuthorization(accountOne, accountTwo, 600, 3);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, sibs.operations.get(0).getState());

	}

	@After
	public void tearDown() {
		Operation.counter = 0;

		SIBS sibs = SIBS.getInstance();
		sibs.operations.clear();
		RegisterAuthorizationOperation.counter = 0;

	}

}
