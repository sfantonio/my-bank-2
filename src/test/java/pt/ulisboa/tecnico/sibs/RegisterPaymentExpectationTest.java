package pt.ulisboa.tecnico.sibs;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.Client;
import pt.ulisboa.tecnico.bank.ServiceAccount;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.sibs.Operation.State;

public class RegisterPaymentExpectationTest {

	private Bank bankOne;
	private Client company;

	@Before
	public void setUp() throws BankException, RemoteAccessException {
		this.bankOne = new Bank();
		this.company = this.bankOne.createClient("Company", "One", 20, "111111111", "987654321", "Avenue 5");
	}

	@Test
	public void successRegister()
			throws AccountException, BankException, PaymentExpectationException, RemoteAccessException {
		final ServiceAccount serviceAccount = new ServiceAccount(this.company);
		final SIBS sibs = SIBS.getInstance();
		sibs.registerPaymentExpectation(serviceAccount, 100);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(0).getState());
	}

	@Test
	public void failRegisterDueAccountNull() throws PaymentExpectationException {
		final ServiceAccount serviceAccount = null;
		final SIBS sibs = SIBS.getInstance();
		sibs.registerPaymentExpectation(serviceAccount, 100);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(0).getState());

	}

	@Test
	public void failRegisterDueAccountInactive()
			throws AccountException, BankException, PaymentExpectationException, RemoteAccessException {
		final ServiceAccount serviceAccount = new ServiceAccount(this.company);
		serviceAccount.makeInactive();
		final SIBS sibs = SIBS.getInstance();
		sibs.registerPaymentExpectation(serviceAccount, 100);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(0).getState());

	}

	@Test
	public void failRegisterDueAmountNull()
			throws AccountException, BankException, PaymentExpectationException, RemoteAccessException {
		final ServiceAccount serviceAccount = new ServiceAccount(this.company);
		final SIBS sibs = SIBS.getInstance();
		sibs.registerPaymentExpectation(serviceAccount, 0);
		sibs.processOperations();

		Assert.assertEquals(1, SIBS.getInstance().operations.size());
		Assert.assertEquals(State.ERROR, SIBS.getInstance().operations.get(0).getState());

	}

	@After
	public void tearDown() {
		Operation.counter = 0;
		SIBS sibs = SIBS.getInstance();
		sibs.operations.clear();
		registerPaymentExpectationOperation.aCounter = 0;
	}

}
