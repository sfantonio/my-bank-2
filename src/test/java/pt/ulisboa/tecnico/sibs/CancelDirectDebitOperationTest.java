package pt.ulisboa.tecnico.sibs;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.Client;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.sibs.Operation.State;

public class CancelDirectDebitOperationTest {
	private Bank bankOne;
	private Client clientOne;
	private Client clientTwo;
	final SIBS sibs = SIBS.getInstance();
	Account accountOne, accountTwo;
	String authorizationId;

	@Before
	public void setUp() throws BankException, AccountException, RemoteAccessException {
		this.bankOne = new Bank("CGD");
		this.clientOne = this.bankOne.createClient("Andre", "Santos", 20, "111111111", "987654321", "Avenue 5");
		this.clientTwo = this.bankOne.createClient("Antonio", "Fernandes", 24, "555555555", "666666666", "Poarto");
		accountOne = bankOne.createAccount(AccountType.CHECKING, clientOne, 0);
		accountTwo = bankOne.createAccount(AccountType.CHECKING, clientTwo, 0);
		accountOne.deposit(1000);
		accountTwo.deposit(1000);
		sibs.RegisterAuthorization(accountOne, accountTwo, 500, 3);
		sibs.processOperations();
		for (Operation op : sibs.getOperations()) {
			if (op instanceof RegisterAuthorizationOperation) {
				authorizationId = ((RegisterAuthorizationOperation) op).getAuthorizationId();
				break;
			}
		}

	}

	@Test
	public void cancelSuccessfulTest() {

		Assert.assertEquals(1, sibs.getOperations().size());

		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(0).getState());
		sibs.CancelDirectDebit(authorizationId);
		sibs.processOperations();
		Assert.assertEquals(State.DONE, SIBS.getInstance().operations.get(1).getState());
		Assert.assertEquals(2, sibs.getOperations().size());
		Assert.assertEquals(accountOne.getBalance(), 1000);
		Assert.assertEquals(accountTwo.getBalance(), 1000);
		sibs.RequestDirectDebit(authorizationId, 500);
		sibs.processOperations();
		Assert.assertEquals(accountOne.getBalance(), 1000);
		Assert.assertEquals(accountTwo.getBalance(), 1000);

	}

	@Test
	public void cancelNullId() {
		sibs.CancelDirectDebit(null);
		sibs.processOperations();
		Assert.assertEquals(State.ERROR, sibs.operations.get(1).getState());
	}

	@Test
	public void cancelNonExistentId() {
		sibs.CancelDirectDebit("3");
		sibs.processOperations();
		Assert.assertEquals(State.ERROR, sibs.operations.get(1).getState());
	}

	@Test
	public void undoCancelTest() {
		sibs.CancelDirectDebit(authorizationId);
		sibs.processOperations();

		sibs.operations.get(1).setState(State.UNDO);
		sibs.processOperations();

		Assert.assertEquals(State.ERROR, sibs.operations.get(1).getState());
	}

	@After
	public void tearDown() {
		Operation.counter = 0;
		RegisterAuthorizationOperation.counter = 0;
		SIBS sibs = SIBS.getInstance();
		sibs.operations.clear();

	}

}
