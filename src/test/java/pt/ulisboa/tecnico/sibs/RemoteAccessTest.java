package pt.ulisboa.tecnico.sibs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mockit.Expectations;
import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.CheckingAccount;
import pt.ulisboa.tecnico.bank.Client;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.sibs.Operation.State;

public class RemoteAccessTest {

	private Bank bankOne, bankTwo;
	private Client clientOne, clientTwo;
	private Account accountOne, accountTwo;

	@Before
	public void setUp() throws Exception {

		this.bankOne = new Bank("CGD");
		this.bankTwo = new Bank("Metrogos");

		clientOne = bankOne.createClient("Antonio", "Fernandes", 45, "111111111", "987123456", "Puarto");
		clientTwo = bankTwo.createClient("Andre", "DSantos", 25, "222222222", "123987456", "OdiCandels");

		accountOne = new CheckingAccount(clientOne);
		accountTwo = new CheckingAccount(clientTwo);

		accountOne.deposit(100);
	}

	@Test
	public void doTransferOperationRemoteAccessExceptionFromAccount() throws AccountException, RemoteAccessException {

		new Expectations(Bank.class) {
			{
				bankOne.getAccountById(accountOne.getAccountId());
				result = new RemoteAccessException();
			}
		};

		SIBS.getInstance().transfer(bankOne, accountOne.getAccountId(), 100, bankTwo, accountTwo.getAccountId());

		assertEquals(SIBS.getInstance().getOperations().size(), 1);
		assertTrue(SIBS.getInstance().getOperations().get(0).getState().equals(State.DO));

		SIBS.getInstance().processOperations();
		assertEquals(State.DO, SIBS.getInstance().getOperations().get(0).getState());

		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();

		assertTrue(SIBS.getInstance().getOperations().get(0).getState().equals(State.ERROR));
	}

	@Test
	public void undoTransferOperationRemoteAccessExceptionToAccount() throws AccountException, RemoteAccessException {

		accountTwo.deposit(100);
		SIBS.getInstance().transfer(bankTwo, accountTwo.getAccountId(), 100, bankOne, accountOne.getAccountId());

		assertEquals(SIBS.getInstance().getOperations().size(), 1);
		assertTrue(SIBS.getInstance().getOperations().get(0).getState().equals(State.DO));

		SIBS.getInstance().processOperations();
		assertTrue(SIBS.getInstance().getOperations().get(0).getState().equals(State.DONE));

		new Expectations(Bank.class) {
			{
				bankTwo.getAccountById(accountTwo.getAccountId());
				result = new RemoteAccessException();
			}
		};

		SIBS.getInstance().getOperations().get(0).setState(State.UNDO);
		SIBS.getInstance().processOperations();

		assertEquals(State.UNDO, SIBS.getInstance().getOperations().get(0).getState());

		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();

		assertEquals(State.UNDO, SIBS.getInstance().getOperations().get(0).getState());
	}

	@Test
	public void doTransferOperationRemoteAccessExceptionToAccount() throws AccountException, RemoteAccessException {
		new Expectations(Bank.class) {
			{
				bankTwo.getAccountById(accountTwo.getAccountId());
				result = new RemoteAccessException();
			}
		};

		SIBS.getInstance().transfer(bankOne, accountOne.getAccountId(), 100, bankTwo, accountTwo.getAccountId());

		assertEquals(SIBS.getInstance().getOperations().size(), 1);
		assertTrue(SIBS.getInstance().getOperations().get(0).getState().equals(State.DO));

		SIBS.getInstance().processOperations();
		assertEquals(State.DO, SIBS.getInstance().getOperations().get(0).getState());

		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();

		assertTrue(SIBS.getInstance().getOperations().get(0).getState().equals(State.ERROR));
	}

	@Test
	public void undoTransferOperationRemoteAccessExceptionFromAccount() throws AccountException, RemoteAccessException {

		SIBS.getInstance().transfer(bankOne, accountOne.getAccountId(), 100, bankTwo, accountTwo.getAccountId());

		new Expectations(Bank.class) {
			{
				bankTwo.getAccountById(accountTwo.getAccountId());
				result = new RemoteAccessException();
			}
		};

		SIBS.getInstance().transfer(bankOne, accountTwo.getAccountId(), 100, bankOne, accountOne.getAccountId());
		SIBS.getInstance().getOperations().get(0).setState(State.UNDO);
		assertEquals(SIBS.getInstance().getOperations().size(), 2);
		assertTrue(SIBS.getInstance().getOperations().get(0).getState().equals(State.UNDO));

		SIBS.getInstance().processOperations();
		assertEquals(100, accountOne.getBalance());
		assertEquals(0, accountTwo.getBalance());

		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();
		SIBS.getInstance().processOperations();

		assertEquals(State.UNDO, SIBS.getInstance().getOperations().get(0).getState());
	}

	@After
	public void tearDown() throws Exception {
		Operation.counter = 0;
		SIBS sibs = SIBS.getInstance();
		sibs.operations.clear();
		Bank.banks.clear();
	}

}
