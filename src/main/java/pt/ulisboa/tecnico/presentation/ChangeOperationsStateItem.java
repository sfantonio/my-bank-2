package pt.ulisboa.tecnico.presentation;

import pt.ulisboa.tecnico.Application;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.sibs.Operation;
import pt.ulisboa.tecnico.sibs.Operation.State;
import pt.ulisboa.tecnico.sibs.SIBS;

public class ChangeOperationsStateItem extends MenuItem {

	public ChangeOperationsStateItem() {
		super("Change Operations State Operation");
		// TODO Auto-generated constructor stub
	}

	@Override
	void doView() {
		System.out.println("Do you want to change the operations ERROR state to DO?(Y|N)");

	}

	@Override
	void doController() throws AccountException, BankException, ClientException, PaymentExpectationException {
		String response = this.cmd;
		if (response.equals("N")) {
			Application.getInstance().setContext(0);
		}
		if (response.equals("Y")) {
			for (Operation operation : SIBS.getInstance().getOperations()) {
				if (operation.getState().equals(State.ERROR)) {
					operation.setState(State.DO);
				}
			}
		}

	}
}
