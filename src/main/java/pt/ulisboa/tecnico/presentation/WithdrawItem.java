package pt.ulisboa.tecnico.presentation;

import java.util.HashSet;
import java.util.Set;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;

public class WithdrawItem extends MenuItem {

	public WithdrawItem() {
		super("Withdraw Operation");

	}

	@Override
	void doView() {
		Set<Account> accounts = new HashSet<>();
		for (Bank bank : Bank.banks) {
			accounts.addAll(bank.getAccountSet());
		}

		for (Account account : accounts) {
			System.out.println("Withdraw(" + account.getAccountId() + "," + account.getBalance() + ")");
		}

	}

	@Override
	void doController() throws AccountException, BankException, ClientException {
		String accountId = this.cmd;
		int amount = this.in.nextInt();

		Account account = null;
		for (Bank bank : Bank.banks) {
			account = bank.getAccountById(accountId);
			if (account != null) {
				break;
			}
		}

		if (account == null) {
			throw new AccountException("Account does not exist", 0);
		}
		account.withdraw(amount);

	}

}
