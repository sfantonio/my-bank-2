package pt.ulisboa.tecnico.presentation;

import pt.ulisboa.tecnico.Application;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.sibs.SIBS;

public class ProcessOperationsItem extends MenuItem {

	public ProcessOperationsItem() {
		super("Process Operations Operation");
		// TODO Auto-generated constructor stub
	}

	@Override
	void doView() {
		System.out.println("Process Operations?(Y|N)");

	}

	@Override
	void doController() throws AccountException, BankException, ClientException, PaymentExpectationException {
		String response = this.cmd;
		if (response.equals("Y")) {
			SIBS.getInstance().processOperations();
		}
		if (response.equals("N")) {
			Application.getInstance().setContext(0);
		}

	}

}
