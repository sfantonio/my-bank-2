package pt.ulisboa.tecnico.presentation;

import java.util.HashSet;
import java.util.Set;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.sibs.Operation;
import pt.ulisboa.tecnico.sibs.PayPaymentExpectationOperation;
import pt.ulisboa.tecnico.sibs.SIBS;
import pt.ulisboa.tecnico.sibs.registerPaymentExpectationOperation;

public class SubmitPaymentItem extends MenuItem {

	public SubmitPaymentItem() {
		super("Submit Payment Operation");
	}

	@Override
	void doView() {

		Set<Account> accounts = new HashSet<>();
		for (Bank bank : Bank.banks) {
			accounts.addAll(bank.getAccountSet());
		}

		for (Account account : accounts) {
			if (!account.getAccountId().startsWith(AccountType.SERVICE.getPrefix())) {
				System.out.println("User Account(" + account.getAccountId() + "," + account.getBalance() + ")");
			}
		}

		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof registerPaymentExpectationOperation) {
				String reference = ((registerPaymentExpectationOperation) op).getReference();
				boolean loop = false;

				for (Operation operation : SIBS.getInstance().getOperations()) {
					if (operation instanceof PayPaymentExpectationOperation) {
						if (((PayPaymentExpectationOperation) operation).getRef().equals(reference)) {
							loop = true;
						}
					}
				}
				if (loop == true) {
					continue;
				}

				System.out.println(
						"(ref: " + reference + ", " + ((registerPaymentExpectationOperation) op).getAmount() + ")");

			}
		}

		System.out.println("(User Account, Payment Reference, Amount)");

	}

	@Override
	void doController() throws AccountException, BankException, PaymentExpectationException {
		String accountId = this.cmd;
		String reference = this.in.next();
		int amount = this.in.nextInt();

		Account account = null;
		for (Bank bank : Bank.banks) {
			account = bank.getAccountById(accountId);
			if (account != null) {
				break;
			}
		}

		SIBS.getInstance().payPaymentExpectation(account, reference, amount);
	}

}
