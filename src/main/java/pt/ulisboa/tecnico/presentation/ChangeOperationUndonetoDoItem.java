package pt.ulisboa.tecnico.presentation;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;
import pt.ulisboa.tecnico.sibs.Operation;
import pt.ulisboa.tecnico.sibs.Operation.State;
import pt.ulisboa.tecnico.sibs.RegisterAuthorizationOperation;
import pt.ulisboa.tecnico.sibs.RequestDirectDebitOperation;
import pt.ulisboa.tecnico.sibs.SIBS;

public class ChangeOperationUndonetoDoItem extends MenuItem {

	public ChangeOperationUndonetoDoItem() {
		super("Change the operation from Undone to Do");

	}

	@Override
	void doView() {
		System.out.println("If you want to change the amount of a direct debit, please insert the id.");
		System.out.println("\t Request Debit Operation Id:");
		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof RequestDirectDebitOperation && op.getState().equals(State.UNDONE)) {
				System.out.println("\t \t ID: " + op.getLogId() + " State: " + op.getState());
			}
		}

	}

	@Override
	void doController()
			throws AccountException, BankException, ClientException, PaymentExpectationException, SIBSException {
		String response = this.cmd;
		int amount = this.in.nextInt();

		for (Operation op : SIBS.getInstance().getOperations()) {
			if (response.equals(Integer.toString(op.getLogId()))) {
				op.setState(State.DO);
				((RegisterAuthorizationOperation) op).setAmount(amount);

				break;

			}
		}

	}

}
