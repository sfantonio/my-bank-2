package pt.ulisboa.tecnico.presentation;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;
import pt.ulisboa.tecnico.sibs.CancelDirectDebitOperation;
import pt.ulisboa.tecnico.sibs.Operation;
import pt.ulisboa.tecnico.sibs.Operation.State;
import pt.ulisboa.tecnico.sibs.PayPaymentExpectationOperation;
import pt.ulisboa.tecnico.sibs.RequestDirectDebitOperation;
import pt.ulisboa.tecnico.sibs.SIBS;
import pt.ulisboa.tecnico.sibs.TransferOperation;
import pt.ulisboa.tecnico.sibs.registerPaymentExpectationOperation;

public class ChangeOperationDoneToUndoItem extends MenuItem {

	public ChangeOperationDoneToUndoItem() {
		super("Change operation state from done to undo");

	}

	@Override
	void doView() {
		System.out.println("Please select the operation you want to undo");

		System.out.println("\t Transfer Operation Id:");
		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof TransferOperation) {
				System.out.println("\t \t ID: " + op.getLogId() + " State: " + op.getState());
			}
		}
		System.out.println("\t Request Debit Operation Id:");
		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof RequestDirectDebitOperation) {
				System.out.println("\t \t ID: " + op.getLogId() + " State: " + op.getState() + ".."
						+ ((RequestDirectDebitOperation) op).getCurrentTransaction());
				System.out.println("\t Authorization Id" + ((RequestDirectDebitOperation) op).getAuthorizationId());
			}
		}
		System.out.println("\t Cancel direct debit Operation Id:");
		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof CancelDirectDebitOperation) {
				System.out.println(" \t \t ID: " + op.getLogId() + " State: " + op.getState());
			}
		}
		System.out.println("\t Pay Payment Expectation Operation Id:");
		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof PayPaymentExpectationOperation) {
				System.out.println("\t \t ID: " + op.getLogId() + " State: " + op.getState());
			}
		}

		System.out.println("\t Register Payment Expectation Operation Id:");
		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof registerPaymentExpectationOperation) {
				System.out.println("\t \t ID: " + op.getLogId() + "State: " + op.getState());
			}
		}

	}

	@Override
	void doController()
			throws AccountException, BankException, ClientException, PaymentExpectationException, SIBSException {

		String response = this.cmd;

		for (Operation op : SIBS.getInstance().getOperations()) {
			if (response.equals(Integer.toString(op.getLogId()))) {
				op.setState(State.UNDO);
				break;

			}
		}
	}
}
