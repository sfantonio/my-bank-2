package pt.ulisboa.tecnico.presentation;

import java.util.ArrayList;
import java.util.List;

import pt.ulisboa.tecnico.Application;
import pt.ulisboa.tecnico.bank.exception.AccountException;

public class CompositeMenu extends Menu {
	List<Menu> items = new ArrayList<>();

	public void addMenu(Menu menu) {
		this.items.add(menu);
	}

	public CompositeMenu(String name) {
		super(name);
	}

	@Override
	void doView() {
		int counter = 0;
		for (Menu item : this.items) {
			System.out.println(++counter + " - " + item.getName());
		}
	}

	@Override
	void doController() throws AccountException {
		int option = Integer.parseInt(this.cmd);

		Application.getInstance().setContext(option);
	}

	public Menu getItem(int option) {
		return this.items.get(option - 1);
	}

	@Override
	public Menu getParentItem(Menu context) {
		if (this.items.contains(context)) {
			return this;
		}

		for (Menu item : this.items) {
			Menu result = item.getParentItem(context);
			if (result != null) {
				return result;
			}
		}

		return null;
	}

}
