package pt.ulisboa.tecnico.presentation;

import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;

public class BankItem extends MenuItem {

	public BankItem() {
		super("Create Bank operation");
		// TODO Auto-generated constructor stub
	}

	@Override
	void doView() {
		for (Bank bank : Bank.banks) {
			System.out.println("Bank name: " + bank.getBankID());

		}
	}

	@Override
	void doController() throws AccountException, BankException {
		String bankName = this.cmd;

		for (Bank bank : Bank.banks) {

			if (bank.getBankID().equals(bankName)) {

				throw new BankException("This bank already exists", bankName);
			}
		}
		Bank bank = new Bank(bankName);

	}

}
