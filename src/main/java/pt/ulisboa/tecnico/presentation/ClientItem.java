package pt.ulisboa.tecnico.presentation;

import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.Client;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class ClientItem extends MenuItem {

	public ClientItem() {
		super("Create Client Operation");
		// TODO Auto-generated constructor stub
	}

	@Override
	void doView() {
		System.out.println("Choose a bank: ");
		System.out.println("Available Banks:  \t");
		for (Bank bank : Bank.banks) {
			System.out.println(bank.getBankID());
		}
		System.out.println("Bank, First Name , Last Name, Age, NIF, PH Number, Address");

	}

	@Override
	void doController() throws AccountException, ClientException, BankException, RemoteAccessException {
		String clientBank = this.cmd;
		String clientFName = this.in.next();
		String clientLNAme = this.in.next();
		int clientAge = this.in.nextInt();
		String clientNIF = this.in.next();
		String clientPN = this.in.next();
		String clientAddress = this.in.next();
		Bank bank = null;
		boolean test = false;

		for (Bank banks : Bank.banks) {
			if (banks.getBankID().equals(clientBank)) {
				bank = banks;
				test = true;
			}
		}

		if (test == false) {
			throw new BankException("Bank does not exist", clientBank);
		}

		Client newClient = new Client(bank, clientFName, clientLNAme, clientAge, clientNIF, clientPN, clientAddress);

		System.out.println("Created client with following info: Bank: " + clientBank + " FName " + clientFName
				+ " LName " + clientLNAme + " Age: " + clientAge + " Nif: " + clientNIF + " Ph Number " + clientPN
				+ " Address: " + clientAddress);

	}

}
