package pt.ulisboa.tecnico.presentation;

import java.util.Scanner;

import pt.ulisboa.tecnico.Application;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;

public abstract class Menu {
	Scanner in = new Scanner(System.in);
	String cmd;

	private final String name;

	public String getName() {
		return this.name;
	}

	public Menu(String name) {
		this.name = name;
	}

	public final void execute()
			throws AccountException, BankException, ClientException, SIBSException, RemoteAccessException {
		view();
		controller();
	}

	final void view() {
		System.out.println("Menu: " + this.getName());
		doView();
		System.out.print(">>");
	}

	final void controller()
			throws AccountException, BankException, ClientException, SIBSException, RemoteAccessException {
		this.cmd = this.in.next();
		if (this.cmd.equals("0")) {
			Application.getInstance().setContext(0);
		} else {
			doController();
		}
		System.out.println("");
	}

	abstract void doView();

	abstract void doController() throws AccountException, BankException, ClientException, PaymentExpectationException,
			SIBSException, RemoteAccessException;

	public abstract Menu getParentItem(Menu context);

	public abstract Menu getItem(int option);

}
