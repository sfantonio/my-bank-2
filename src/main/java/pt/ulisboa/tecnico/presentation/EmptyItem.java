package pt.ulisboa.tecnico.presentation;

import pt.ulisboa.tecnico.bank.exception.AccountException;

public class EmptyItem extends MenuItem {

	public EmptyItem() {
		super("Empty Operation");
	}

	@Override
	void doView() {
		System.out.println("* - I'm lazy. I do nothing!");
	}

	@Override
	void doController() throws AccountException {
		System.out.println("Yes, I understand, it is " + this.cmd);
	}

}
