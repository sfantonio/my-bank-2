package pt.ulisboa.tecnico.presentation;

import java.util.HashSet;
import java.util.Set;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.exception.SIBSException;
import pt.ulisboa.tecnico.sibs.CancelDirectDebitOperation;
import pt.ulisboa.tecnico.sibs.Operation;
import pt.ulisboa.tecnico.sibs.RegisterAuthorizationOperation;
import pt.ulisboa.tecnico.sibs.SIBS;

public class RegisterAuthorizationItem extends MenuItem {

	Account clientAccount;
	String clientAccountId;
	Account providerAccount;
	String providerAccountId;
	int transactionAmount;
	int numberOfTransactions;

	public RegisterAuthorizationItem() {
		super("Register authorization");
	}

	@Override
	void doView() {
		Set<Account> accounts = new HashSet<>();
		for (Bank bank : Bank.banks) {
			accounts.addAll(bank.getAccountSet());
		}

		System.out.println(
				"In order to register a payment authorization, please insert: clientAccount providerAccount transactionAmount numberOfTransactions");
		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof RegisterAuthorizationOperation) {
				String reference = ((RegisterAuthorizationOperation) op).getAuthorizationId();
				boolean loop = false;

				for (Operation operation : SIBS.getInstance().getOperations()) {
					if (operation instanceof CancelDirectDebitOperation) {
						if (((CancelDirectDebitOperation) operation).getAuthorizationID().equals(reference)) {
							loop = true;
						}
					}
				}
				if (loop == true) {
					continue;
				}

				System.out.println("(ref: " + ((RegisterAuthorizationOperation) op).getAuthorizationId() + ", "
						+ ((RegisterAuthorizationOperation) op).getTransactionAmount() + ")");
			}
		}

	}

	@Override
	void doController() throws SIBSException {
		String clientAccountId = this.cmd;
		String providerAccountId = this.in.next();
		int transactionAmount = this.in.nextInt();
		int numberOfTransactions = this.in.nextInt();

		boolean aux = false;
		for (Bank bank : Bank.banks) {
			for (Account account : bank.getAccountSet()) {
				if (account.getAccountId().equals(clientAccountId)) {
					clientAccount = bank.getAccountById(clientAccountId);
					aux = true;
					break;
				}
			}
		}
		if (aux == false)
			throw new SIBSException("The client ID is invalid", clientAccountId);

		aux = false;
		for (Bank bank : Bank.banks) {
			for (Account account : bank.getAccountSet()) {
				if (account.getAccountId().equals(providerAccountId)) {
					providerAccount = bank.getAccountById(providerAccountId);
					aux = true;
					break;

				}
			}
		}
		if (aux == false)
			throw new SIBSException("The provider ID is invalid", providerAccountId);

		SIBS.getInstance().RegisterAuthorization(clientAccount, providerAccount, transactionAmount,
				numberOfTransactions);

	}

}
