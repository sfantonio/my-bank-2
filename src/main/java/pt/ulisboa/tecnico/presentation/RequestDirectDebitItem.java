package pt.ulisboa.tecnico.presentation;

import pt.ulisboa.tecnico.sibs.CancelDirectDebitOperation;
import pt.ulisboa.tecnico.sibs.Operation;
import pt.ulisboa.tecnico.sibs.RegisterAuthorizationOperation;
import pt.ulisboa.tecnico.sibs.SIBS;

public class RequestDirectDebitItem extends MenuItem {

	public RequestDirectDebitItem() {
		super("Request Direct Debit Operation");
		// TODO Auto-generated constructor stub
	}

	@Override
	void doView() {
		System.out.println("Select a direct debit reference and the amount");

		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof RegisterAuthorizationOperation) {
				String reference = ((RegisterAuthorizationOperation) op).getAuthorizationId();
				boolean loop = false;

				for (Operation operation : SIBS.getInstance().getOperations()) {
					if (operation instanceof CancelDirectDebitOperation) {
						if (((CancelDirectDebitOperation) operation).getAuthorizationID().equals(reference)) {
							loop = true;
						}
					}
				}
				if (loop == true) {
					continue;
				}

				System.out.println("(ref: " + ((RegisterAuthorizationOperation) op).getAuthorizationId() + ", "
						+ ((RegisterAuthorizationOperation) op).getTransactionAmount() + ")");

				System.out.println("Select a reference and an amount.");

			}
		}

	}

	@Override
	void doController() {
		String reference = this.cmd;
		int amount = this.in.nextInt();

		SIBS.getInstance().RequestDirectDebit(reference, amount);

	}

}
