package pt.ulisboa.tecnico.presentation;

import java.util.HashSet;
import java.util.Set;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.Client;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class CreateAccountItem extends MenuItem {

	public CreateAccountItem() {
		super("Create account operation");

	}

	@Override
	void doView() {
		Set<Account> accounts = new HashSet<>();
		for (Bank bank : Bank.banks) {
			accounts.addAll(bank.getAccountSet());
		}

		for (Account account : accounts) {
			System.out.println("These are the existing accounts " + account.getAccountId());
		}

		System.out.println("You can choose these type of account:");
		System.out.println("1-" + " " + Account.AccountType.CHECKING + " - CK");
		System.out.println("2-" + " " + Account.AccountType.SALARY + " - SL");
		System.out.println("3-" + " " + Account.AccountType.SAVINGS + " - SV");
		System.out.println("4-" + " " + Account.AccountType.SERVICE + " - SRVC");

		System.out.println("In order to create an account insert: Bank Name, Client NIF, Account Prefix, Value");

	}

	@Override
	void doController() throws AccountException, BankException, RemoteAccessException {
		String bankName = this.cmd;
		Client client = null;
		String NIF = this.in.next();
		Bank bank = null;

		for (Bank b : Bank.banks) {
			if (b.getBankID().equals(bankName)) {
				bank = b;
				client = b.getClientByNIF(NIF);
			}
		}
		if (bank == null) {
			throw new BankException("This bank does not exist", bankName);

		}
		if (client == null) {
			throw new BankException("This bank is not associated with the client", bankName);

		}

		if (bank.getClientByNIF(NIF) == null) {
			throw new BankException("Client not found", NIF);
		}

		String accountId = this.in.next();
		int value = this.in.nextInt();
		Account account;
		switch (accountId) {
		case "CK":
			account = bank.createAccount(AccountType.CHECKING, client, value);
			break;
		case "SV":
			account = bank.createAccount(AccountType.SAVINGS, client, value);
			break;
		case "SL":
			account = bank.createAccount(AccountType.SALARY, client, value);
			break;
		case "SRVC":
			account = bank.createAccount(AccountType.SERVICE, client, value);
			break;
		default:
			throw new BankException("Create Account: Undefined Prefix for the account type",
					client.getFirstName() + " " + client.getLastName());
		}

	}

}
