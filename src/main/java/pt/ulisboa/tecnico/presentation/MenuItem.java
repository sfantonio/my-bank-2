package pt.ulisboa.tecnico.presentation;

public abstract class MenuItem extends Menu {

	public MenuItem(String name) {
		super(name);
	}

	@Override
	public Menu getItem(int option) {
		return this;
	}

	@Override
	public Menu getParentItem(Menu context) {
		return null;
	}

}
