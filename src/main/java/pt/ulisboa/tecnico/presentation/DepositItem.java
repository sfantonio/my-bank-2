package pt.ulisboa.tecnico.presentation;

import java.util.HashSet;
import java.util.Set;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.exception.AccountException;

public class DepositItem extends MenuItem {

	public DepositItem() {
		super("Deposit Operation");
	}

	@Override
	void doView() {
		Set<Account> accounts = new HashSet<>();
		for (Bank bank : Bank.banks) {
			accounts.addAll(bank.getAccountSet());
		}

		for (Account account : accounts) {
			System.out.println("Account(" + account.getAccountId() + "," + account.getBalance() + ")");
		}
	}

	@Override
	void doController() throws AccountException {
		String accountId = this.cmd;
		int amount = this.in.nextInt();

		Account account = null;
		for (Bank bank : Bank.banks) {
			account = bank.getAccountById(accountId);
			if (account != null) {
				break;
			}
		}

		if (account == null) {
			throw new AccountException("Account does not exist", 0);
		}
		account.deposit(amount);

	}

}
