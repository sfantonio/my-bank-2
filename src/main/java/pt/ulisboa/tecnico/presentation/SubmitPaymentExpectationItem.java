package pt.ulisboa.tecnico.presentation;

import java.util.HashSet;
import java.util.Set;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.ServiceAccount;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.sibs.SIBS;

public class SubmitPaymentExpectationItem extends MenuItem {

	public SubmitPaymentExpectationItem() {
		super("Submit Payment Expectation Operation");
		// TODO Auto-generated constructor stub
	}

	@Override
	void doView() {
		System.out.println("(Service Account, ammount)");
		Set<Account> accounts = new HashSet<>();
		for (Bank bank : Bank.banks) {
			accounts.addAll(bank.getAccountSet());
		}
		for (Account account : accounts) {
			if (account.getAccountId().startsWith(AccountType.SERVICE.getPrefix())) {
				System.out.println("Account(" + account.getAccountId() + ")");
			}
		}
	}

	@Override
	void doController() throws AccountException, BankException, ClientException, PaymentExpectationException {
		String accountService = this.cmd;
		int amount = this.in.nextInt();

		ServiceAccount servAccount = null;

		for (Bank bank : Bank.banks) {
			servAccount = (ServiceAccount) bank.getAccountById(accountService);
			if (servAccount != null) {
				break;
			}
		}

		SIBS.getInstance().registerPaymentExpectation(servAccount, amount);
	}

}
