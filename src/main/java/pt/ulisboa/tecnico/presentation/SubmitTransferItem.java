package pt.ulisboa.tecnico.presentation;

import java.util.HashSet;
import java.util.Set;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.sibs.SIBS;

public class SubmitTransferItem extends MenuItem {

	public SubmitTransferItem() {
		super("Submit Transfer Operation");
		// TODO Auto-generated constructor stub
	}

	@Override
	void doView() {
		Set<Account> accounts = new HashSet<>();
		System.out.println("(AccountFrom, AccountTo, Value)");
		for (Bank bank : Bank.banks) {
			System.out.println("\t" + bank.getBankID());
			accounts.addAll(bank.getAccountSet());
			for (Account account : accounts) {
				System.out.println("\t \t" + "Account(" + account.getAccountId() + "," + account.getBalance() + ")");
			}
			accounts.removeAll(bank.getAccountSet());
		}

	}

	@Override
	void doController() throws AccountException, BankException, ClientException {
		String accountFromId = this.cmd;
		String accountToId = this.in.next();
		int amount = this.in.nextInt();

		Account accountFrom = null;
		Account accountTo = null;
		for (Bank bank : Bank.banks) {
			accountFrom = bank.getAccountById(accountFromId);
			if (accountFrom != null) {
				break;
			}
		}
		if (accountFrom == null) {
			throw new AccountException("AccountFrom does not exist", 0);
		}
		for (Bank bank : Bank.banks) {
			accountTo = bank.getAccountById(accountToId);
			if (accountTo != null) {
				break;
			}
		}
		if (accountTo == null) {
			throw new AccountException("AccountTo does not exist", 0);
		}
		SIBS.getInstance().transfer(accountFrom.getBank(), accountFrom.getAccountId(), amount, accountTo.getBank(),
				accountTo.getAccountId());
	}

}
