package pt.ulisboa.tecnico.sibs;

import java.util.Date;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.SavingsAccount;
import pt.ulisboa.tecnico.bank.YoungAccount;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;

public class TransferOperation extends Operation {
	private final Bank fromBank;
	private final String fromAccountId;
	private final int amount;
	private final Bank toBank;
	private final String toAccountId;

	public TransferOperation(Bank fromBank, String fromAccountId, int amount, Bank toBank, String toAccountId) {
		super(new Date());

		this.fromBank = fromBank;
		this.fromAccountId = fromAccountId;
		this.amount = amount;
		this.toBank = toBank;
		this.toAccountId = toAccountId;
	}

	@Override
	public void doExecute() throws SIBSException, RemoteAccessException {

		if (this.fromBank == this.toBank) {
			throw new SIBSException("transfer: trying to transfer inside the same bank", this.fromBank.toString());
		}

		Account fromAccount = this.fromBank.getAccountById(this.fromAccountId);
		Account toAccount = this.toBank.getAccountById(this.toAccountId);

		if (fromAccount == null) {
			throw new SIBSException("transfer: account does not belong to bank", this.fromAccountId);
		}

		if (toAccount == null) {
			throw new SIBSException("transfer: account does not belong to bank", this.toAccountId);
		}

		if (!toAccount.isActive()) {
			throw new SIBSException("transfer: the account is not active", this.toAccountId);
		}

		if (toAccount.getAccountId().startsWith(AccountType.SAVINGS.getPrefix())
				&& this.amount % ((SavingsAccount) toAccount).getBase() != 0) {
			throw new SIBSException("transfer: the account is not a multiple of base", Integer.toString(this.amount));
		}

		if (toAccount.getAccountId().startsWith(AccountType.YOUNG.getPrefix())
				&& this.amount % ((YoungAccount) toAccount).getBase() != 0) {
			throw new SIBSException("transfer: the account is not a multiple of base", Integer.toString(this.amount));
		}

		try {
			fromAccount.withdraw(this.amount);
		} catch (AccountException e) {
			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}

		try {
			toAccount.deposit(this.amount);
		} catch (AccountException e) {
			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}

	}

	public Bank getFromBank() {
		return this.fromBank;
	}

	public String getFromAccountId() {
		return this.fromAccountId;
	}

	public int getAmount() {
		return this.amount;
	}

	public Bank getToBank() {
		return this.toBank;
	}

	public String getToAccountId() {
		return this.toAccountId;
	}

	@Override
	public void undoExecute() throws SIBSException {
		Account fromAccount = this.fromBank.getAccountById(this.fromAccountId);
		Account toAccount = this.toBank.getAccountById(this.toAccountId);

		try {
			toAccount.undoWithdraw(amount);
		} catch (AccountException e) {
			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}
		try {
			fromAccount.undoDeposit(amount);
		} catch (AccountException e) {
			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}

	}

}
