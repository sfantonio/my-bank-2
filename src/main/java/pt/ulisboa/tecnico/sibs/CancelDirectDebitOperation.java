package pt.ulisboa.tecnico.sibs;

import java.util.Date;

import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;

public class CancelDirectDebitOperation extends Operation {

	private final String authorizationId;

	public CancelDirectDebitOperation(String reference) {
		super(new Date());
		this.authorizationId = reference;
	}

	public String getAuthorizationID() {
		return this.authorizationId;
	}

	@Override
	public void doExecute() throws SIBSException, RemoteAccessException {

		if (this.authorizationId == null) {
			throw new SIBSException("ID must be valid", this.authorizationId);
		}

		boolean aux = false;

		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof RegisterAuthorizationOperation) {
				if (((RegisterAuthorizationOperation) op).getAuthorizationId().equals(this.authorizationId)) {
					aux = true;
					break;
				}

			}
		}
		if (aux == false) {
			throw new SIBSException("ID must exist", this.authorizationId);
		}
	}

	@Override
	public void undoExecute() throws SIBSException {
		throw new SIBSException("Cannot undo Direct Debit Operations. Register DIrect Debit Authorization",
				authorizationId);

	}

}
