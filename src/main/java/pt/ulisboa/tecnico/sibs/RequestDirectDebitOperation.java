package pt.ulisboa.tecnico.sibs;

import java.util.Date;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;

public class RequestDirectDebitOperation extends Operation {

	private final String authorizationId;
	private int amount;
	Account clientAccount;
	Account providerAccount;
	RegisterAuthorizationOperation registerOperation;

	public RequestDirectDebitOperation(String authorizationID, int amount) {
		super(new Date());
		this.authorizationId = authorizationID;
		this.amount = amount;

	}

	public String getAuthorizationId() {
		return this.authorizationId;
	}

	public int getCurrentTransaction() {
		int number = 0;
		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof RegisterAuthorizationOperation) {
				if (this.authorizationId.equals(((RegisterAuthorizationOperation) op).getAuthorizationId())) {
					number = ((RegisterAuthorizationOperation) op).getCurrentTransactions();
				}
			}
		}
		return number;
	}

	@Override
	public void doExecute() throws SIBSException, RemoteAccessException {

		if (this.authorizationId == null) {
			throw new SIBSException("ID must be valid", this.authorizationId);
		}

		if (this.amount < 0) {
			throw new SIBSException("Amount must be positive", this.authorizationId);
		}

		boolean aux = false;
		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof RegisterAuthorizationOperation) {
				if (((RegisterAuthorizationOperation) op).getAuthorizationId().equals(this.authorizationId)) {
					registerOperation = (RegisterAuthorizationOperation) op;
					aux = true;
				}
			}
		}

		if (aux == false) {
			throw new SIBSException("Invalid id", authorizationId);
		}

		for (Operation op : SIBS.getInstance().getOperations()) {
			if (op instanceof RegisterAuthorizationOperation) {
				if (this.amount > ((RegisterAuthorizationOperation) op).getTransactionAmount()) {
					throw new SIBSException("Amount must not exceed maximum value imposed", this.authorizationId);
				}
			}

		}

		for (Operation op : SIBS.getInstance().operations) {
			if (op instanceof CancelDirectDebitOperation) {
				if (((CancelDirectDebitOperation) op).getAuthorizationID().equals(this.authorizationId)) {
					throw new SIBSException("AUthorization Canceled", this.authorizationId);
				}
			}
		}
		registerOperation.getCurrentTransactions();
		if (registerOperation.getCurrentTransactions() >= registerOperation.getMaxTransactions()) {
			throw new SIBSException("Can't perform more transactions", registerOperation.getAuthorizationId());
		}

		if (this.amount > (registerOperation).getTransactionAmount()) {
			throw new SIBSException("Amount must not exceed maximum value imposed", this.authorizationId);
		}

		this.clientAccount = registerOperation.getClientAccount();

		this.providerAccount = registerOperation.getProviderAccount();

		try {
			clientAccount.withdraw(amount);
		} catch (AccountException e) {

			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}

		try {
			providerAccount.deposit(amount);
		} catch (AccountException e) {

			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}

		registerOperation.incrementTransaction();

	}

	@Override
	public void undoExecute() throws SIBSException {
		try {
			clientAccount.undoDeposit(amount);
		} catch (AccountException e) {

			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}

		try {
			providerAccount.undoWithdraw(amount);
		} catch (AccountException e) {

			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}

		registerOperation.decreaseTransaction();

	}

}
