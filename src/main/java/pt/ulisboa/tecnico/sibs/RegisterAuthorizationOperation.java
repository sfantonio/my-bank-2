package pt.ulisboa.tecnico.sibs;

import java.util.Date;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;

public class RegisterAuthorizationOperation extends Operation {
	Account clientAccount;
	String cliendId;
	Account providerAccount;
	String providerAccountId;
	int maxAmount;
	int numberOfTransactions;
	public static int counter = 0;
	private final String authorizationId;
	private int currentTransaction;

	public RegisterAuthorizationOperation(Account clientAccount, Account providerAccount, int maxAmount,
			int numberOfTransactions) {
		super(new Date());
		this.clientAccount = clientAccount;
		this.providerAccount = providerAccount;
		this.maxAmount = maxAmount;
		this.numberOfTransactions = numberOfTransactions;
		counter = counter + 1;
		this.authorizationId = Integer.toString(counter);
	}

	public void setAmount(int amount) {
		this.maxAmount = amount;
	}

	public String getAuthorizationId() {
		return this.authorizationId;
	}

	public int getTransactionAmount() {
		return this.maxAmount;
	}

	public int getMaxTransactions() {
		return numberOfTransactions;
	}

	public int getCurrentTransactions() {
		return this.currentTransaction;
	}

	public void incrementTransaction() {
		this.currentTransaction++;
	}

	public void decreaseTransaction() {
		this.currentTransaction--;
	}

	public Account getClientAccount() {
		return this.clientAccount;
	}

	public Account getProviderAccount() {
		return this.providerAccount;
	}

	@Override
	public void doExecute() throws SIBSException, RemoteAccessException {
		if (clientAccount == null) {
			throw new SIBSException("Client account doesn't exist", "0");
		}

		if (providerAccount == null) {
			throw new SIBSException("Provider account doesn't exist", "0");
		}

		if (maxAmount <= 0) {
			throw new SIBSException("Amount must be higher than 0", Integer.toString(maxAmount));

		}

		if (providerAccount.getClient().getNIF().equals(clientAccount.getClient().getNIF())) {

			throw new SIBSException("Can not perform direct debits of accounts with the same client",
					providerAccount.getClient().getNIF());
		}

		if (!clientAccount.isActive()) {
			throw new SIBSException("Client Account not active", clientAccount.toString());
		}
		if (!providerAccount.isActive()) {
			throw new SIBSException("Provider Account not active", providerAccount.toString());
		}

	}

	@Override
	public void undoExecute() throws SIBSException {
		throw new SIBSException("Cannot undo Direct Debit Operations. Use Cancel Instead", authorizationId);

	}

}
