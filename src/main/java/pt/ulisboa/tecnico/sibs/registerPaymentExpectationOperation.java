package pt.ulisboa.tecnico.sibs;

import java.util.Date;

import pt.ulisboa.tecnico.bank.ServiceAccount;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;

public class registerPaymentExpectationOperation extends Operation {
	private final ServiceAccount serviceAccount;
	private String reference = null;
	private final int amount;
	private static int number;
	static int aCounter = 0;

	public registerPaymentExpectationOperation(ServiceAccount serviceAccount, int amount)
			throws PaymentExpectationException {
		super(new Date());
		this.serviceAccount = serviceAccount;
		aCounter = aCounter + 1;
		this.reference = Integer.toString(100000000 + aCounter);
		this.amount = amount;

	}

	public int getACounter() {
		return this.aCounter;
	}

	public static void setACounter() {
		aCounter = 0;
	}

	public int getAmount() {
		return this.amount;
	}

	public String getReference() {
		return reference;
	}

	public int getAmmount() {
		return this.amount;
	}

	public ServiceAccount getServiceAccount() {
		return serviceAccount;
	}

	@Override
	public void doExecute() throws SIBSException, RemoteAccessException, PaymentExpectationException {

		if (serviceAccount == null) {
			throw new PaymentExpectationException("Account doesn't exist", "0");
		}

		if (this.reference.length() != 9 || !this.reference.matches("[0-9]+")) {
			throw new PaymentExpectationException("Identifier must have 9 digits", this.reference);
		}
		if (amount <= 0) {
			throw new PaymentExpectationException("Amount must be higher than 0", Integer.toString(amount));

		}

		if (!serviceAccount.isActive()) {
			throw new PaymentExpectationException("Account not active", reference);
		}

		for (Operation op : SIBS.getInstance().operations) {
			if (op instanceof registerPaymentExpectationOperation) {
				if (((registerPaymentExpectationOperation) op).getReference().equals(reference)) {
					if (op.getState().equals(State.DONE)) {
						throw new PaymentExpectationException("Reference already exists", this.reference);
					}

				}
			}
		}
	}

	@Override
	public void undoExecute() throws SIBSException {
		// TODO Auto-generated method stub

	}

}
