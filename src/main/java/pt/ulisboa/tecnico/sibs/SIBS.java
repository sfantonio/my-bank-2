package pt.ulisboa.tecnico.sibs;

import java.util.ArrayList;
import java.util.List;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.ServiceAccount;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;

public class SIBS {
	private static SIBS instance = null;
	private static int referenceCounter = 0;
	private static int authorizationCounter = 0;

	public static SIBS getInstance() {
		if (instance == null) {
			instance = new SIBS();
		}
		return instance;
	}

	final List<Operation> operations = new ArrayList<>();

	private SIBS() {
	}

	public void transfer(Bank fromBank, String fromAccountId, int amount, Bank toBank, String toAccountId) {
		new TransferOperation(fromBank, fromAccountId, amount, toBank, toAccountId);
	}

	public void registerPaymentExpectation(ServiceAccount serviceAccount, int amount)
			throws PaymentExpectationException {
		new registerPaymentExpectationOperation(serviceAccount, amount);

	}

	public void payPaymentExpectation(Account userAccount, String reference, int amount) {
		new PayPaymentExpectationOperation(userAccount, reference, amount);
	}

	public void RegisterAuthorization(Account clientAccount, Account providerAccount, int transactionAmount,
			int numberOfTransactions) {
		new RegisterAuthorizationOperation(clientAccount, providerAccount, transactionAmount, numberOfTransactions);
	}

	public void RequestDirectDebit(String reference, int amount) {
		new RequestDirectDebitOperation(reference, amount);
	}

	public void CancelDirectDebit(String reference) {
		new CancelDirectDebitOperation(reference);
	}

	public void processOperations() {
		for (Operation operation : this.operations) {
			operation.execute();
		}
	}

	public List<Operation> getOperations() {
		return operations;
	}
}
