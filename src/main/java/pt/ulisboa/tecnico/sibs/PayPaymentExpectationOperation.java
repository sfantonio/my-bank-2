package pt.ulisboa.tecnico.sibs;

import java.util.Date;

import pt.ulisboa.tecnico.bank.Account;
import pt.ulisboa.tecnico.bank.ServiceAccount;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;

public class PayPaymentExpectationOperation extends Operation {
	private final Account account;
	private ServiceAccount sa;
	private final String reference;
	private final int amount;

	public PayPaymentExpectationOperation(Account userAccount, String reference, int amount) {

		super(new Date());

		this.account = userAccount;
		this.reference = reference;
		this.amount = amount;
	}

	public String getRef() {
		return this.reference;
	}

	@Override
	public void doExecute() throws SIBSException, RemoteAccessException, PaymentExpectationException {

		for (Operation op : SIBS.getInstance().operations) {
			if (op instanceof registerPaymentExpectationOperation) {
				if (this.reference.equals(((registerPaymentExpectationOperation) op).getReference())) {
					if (!op.getState().equals(State.DONE)) {
						throw new PaymentExpectationException("payment not expected", reference);
					}
				}
			}
		}
		for (Operation op : SIBS.getInstance().operations) {
			if (op instanceof PayPaymentExpectationOperation && op.getState().equals(State.DONE)
					&& this.reference.equals(((PayPaymentExpectationOperation) op).getRef())) {

				throw new PaymentExpectationException("payment already paid", reference);
			}
		}

		for (Operation op : SIBS.getInstance().operations) {
			if (op instanceof registerPaymentExpectationOperation) {
				if (((registerPaymentExpectationOperation) op).getAmount() != amount) {
					throw new PaymentExpectationException("Invalid amount", reference);
				}
			}
		}

		try {
			account.withdraw(this.amount);
		} catch (AccountException e) {
			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}

		for (Operation op : SIBS.getInstance().operations) {
			if (op instanceof registerPaymentExpectationOperation) {
				sa = ((registerPaymentExpectationOperation) op).getServiceAccount();
				try {
					sa.deposit(this.amount);
				} catch (AccountException e) {
					throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
				}
			}
		}
	}

	@Override
	public void undoExecute() throws SIBSException {
		try {
			account.undoDeposit(amount);
		} catch (AccountException e) {

			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}

		try {
			sa.undoWithdraw(amount);
		} catch (AccountException e) {

			throw new SIBSException(e.getError(), Integer.toString(e.getValue()));
		}

	}

}
