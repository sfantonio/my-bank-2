package pt.ulisboa.tecnico.sibs;

import java.util.Date;

import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.bank.exception.SIBSException;

public abstract class Operation {
	public static enum State {
		DO, DONE, UNDO, UNDONE, ERROR
	}

	static int counter = 0;

	public static int getCounter() {
		return Operation.counter;
	}

	public static int MAX_RETRY = 4;

	private final int logId;
	private final Date submissionDate;
	private Date executionDate;
	private State state;
	private int retry;

	public Operation(Date submissionDate) {
		this.logId = ++Operation.counter;
		this.submissionDate = submissionDate;
		this.state = State.DO;
		SIBS.getInstance().operations.add(this);
	}

	public final void execute() {
		if (this.state == State.DO) {
			try {
				doExecute();

				this.state = State.DONE;
				this.executionDate = new Date();
			} catch (SIBSException e) {
				this.state = State.ERROR;
			} catch (RemoteAccessException e) {
				if (this.retry == MAX_RETRY) {
					this.state = State.ERROR;
				} else {
					this.retry++;
				}
			}
		}
		if (this.state == State.UNDO) {
			try {
				undoExecute();
				this.state = State.UNDONE;
				this.executionDate = new Date();
			} catch (SIBSException e) {
				this.state = State.ERROR;
			} catch (RemoteAccessException e) {
				this.retry++;
			}
		}
	}

	public abstract void doExecute() throws SIBSException, RemoteAccessException;

	public abstract void undoExecute() throws SIBSException, RemoteAccessException;

	public int getLogId() {
		return this.logId;
	}

	public Date getSubmissionDate() {
		return this.submissionDate;
	}

	public Date getExecutionDate() {
		return this.executionDate;
	}

	public State getState() {
		return this.state;
	}

	public void setState(State do1) {
		this.state = do1;

	}

}
