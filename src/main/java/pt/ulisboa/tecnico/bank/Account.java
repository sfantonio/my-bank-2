package pt.ulisboa.tecnico.bank;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public abstract class Account {
	public enum AccountType {
		CHECKING("CK"), SAVINGS("SV"), SALARY("SL"), YOUNG("YG"), SERVICE("SRVC");

		private final String prefix;

		AccountType(String prefix) {
			this.prefix = prefix;
		}

		public String getPrefix() {
			return this.prefix;
		}
	}

	protected abstract String getNextAccountId();

	protected void checkCanCreateAccount(Client client) throws AccountException {
		if (client.getAge() < 18) {
			throw new AccountException("checkCanCreateAccount: the client should be over 18", client.getAge());
		}
	}

	private boolean active;

	private final Client client;
	private int balance;
	private final String id;

	public Account(Client client) throws AccountException, BankException, RemoteAccessException {
		checkCanCreateAccount(client);
		this.active = true;
		this.client = client;
		this.id = getNextAccountId();
		client.addAccount(this);
		getBank().addAccount(this);
	}

	public Account(Client client, int balance) throws AccountException, BankException, RemoteAccessException {
		this(client);
		this.balance = balance;
	}

	public boolean isActive() {
		return this.active;
	}

	public void makeInactive() throws AccountException {
		if (this.balance != 0) {
			throw new AccountException("makeInactive: balance different from zero", this.balance);
		}

		this.active = false;
	}

	public Client getClient() {
		return this.client;
	}

	public Bank getBank() {
		return this.client.getBank();
	}

	public String getOwnerName() {
		return this.client.getFirstName() + " " + this.client.getLastName();
	}

	public void deposit(int amount) throws AccountException {
		if (!isActive()) {
			throw new AccountException("deposit: inative account", amount);
		}

		checkNegativeAmount(amount);

		this.balance = this.balance + amount;
	}

	protected void checkNegativeAmount(int amount) throws AccountException {
		if (amount < 0) {
			throw new AccountException("deposit: negative value", amount);
		}
	}

	public void withdraw(int amount) throws AccountException {
		if (!isActive()) {
			throw new AccountException("withdraw: inative account", amount);
		}
		this.balance = this.balance - amount;
	}

	public int getBalance() {
		return this.balance;
	}

	protected void setBalance(int balance) {
		this.balance = balance;
	}

	public String getAccountId() {
		return this.id;
	}

	public void undoWithdraw(int amount) throws AccountException {
		if (!isActive()) {
			throw new AccountException("withdraw: inative account", amount);
		}

		this.balance = this.balance - amount;

	}

	public void undoDeposit(int amount) throws AccountException {
		if (!isActive()) {
			throw new AccountException("withdraw: inative account", amount);
		}

		this.balance = this.balance + amount;
	}

}
