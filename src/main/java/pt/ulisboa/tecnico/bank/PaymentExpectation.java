package pt.ulisboa.tecnico.bank;

import pt.ulisboa.tecnico.bank.exception.PaymentExpectationException;

public class PaymentExpectation {
	private final ServiceAccount serviceAccount;
	private final int amount;
	private final String paymentReference;

	public PaymentExpectation(String reference, int amount, ServiceAccount serviceAccount)
			throws PaymentExpectationException {

		this.paymentReference = reference;
		this.amount = amount;
		this.serviceAccount = serviceAccount;

	}

	public String getAccountID() {
		return this.serviceAccount.getAccountId();
	}

	public int getAmmount() {
		return this.amount;
	}

	public String getPaymentReference() {
		return this.paymentReference;
	}

	public Account getAccount() {
		return this.serviceAccount;
	}

}