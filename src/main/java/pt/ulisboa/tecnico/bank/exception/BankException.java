package pt.ulisboa.tecnico.bank.exception;

public class BankException extends Exception {
	private static final long serialVersionUID = 1L;

	private final String error;
	private final String value;

	public BankException(String error, String value) {
		super(error);
		this.error = error;
		this.value = value;
	}

	public String getError() {
		return error;
	}

	public String getValue() {
		return value;
	}

}
