package pt.ulisboa.tecnico.bank.exception;

public class PaymentExpectationException extends SIBSException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String identifier;
	String error;

	public PaymentExpectationException(String error, String identifier) {
		super(error, identifier);
		this.error = error;
		this.identifier = identifier;
	}
}
