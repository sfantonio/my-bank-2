package pt.ulisboa.tecnico.bank.exception;

public class SIBSException extends Exception {
	private static final long serialVersionUID = 1L;

	private final String message;
	private final String value;

	public SIBSException(String message, String value) {
		super(message);
		this.message = message;
		this.value = value;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	public String getValue() {
		return this.value;
	}

}
