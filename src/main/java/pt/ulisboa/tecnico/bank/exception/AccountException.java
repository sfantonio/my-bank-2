package pt.ulisboa.tecnico.bank.exception;

public class AccountException extends Exception {
	private static final long serialVersionUID = 1L;

	private final String error;
	private final int value;

	public AccountException(String error, int value) {
		super(error);
		this.error = error;
		this.value = value;
	}

	public String getError() {
		return this.error;
	}

	public int getValue() {
		return this.value;
	}

}
