package pt.ulisboa.tecnico.bank.exception;

public class ClientException extends Exception {
	private static final long serialVersionUID = 1L;

	private String message;
	private String value;

	public ClientException(String message, String value) {
		super(message);
		this.message = message;
		this.value = value;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	public String getValue() {
		return this.value;
	}

}
