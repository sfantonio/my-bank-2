package pt.ulisboa.tecnico.bank;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class CheckingAccount extends Account {
	protected static int counter = 0;

	public CheckingAccount(Client client) throws AccountException, BankException, RemoteAccessException {
		super(client);
	}

	public CheckingAccount(Client client, int balance) throws AccountException, BankException, RemoteAccessException {
		super(client, balance);
	}

	@Override
	public String getNextAccountId() {
		return AccountType.CHECKING.getPrefix() + Integer.toString(++CheckingAccount.counter);
	}

	@Override
	public void withdraw(int amount) throws AccountException {
		checkNegativeAmount(amount);

		if (getClient().getCheckingAccountsBalance() - amount < 0) {
			throw new AccountException("withdraw: not enough balance in the sum of all checking accounts of the client",
					amount);
		}

		super.withdraw(amount);

	}

	@Override
	public void undoWithdraw(int amount) throws AccountException {
		checkNegativeAmount(amount);

		if (getClient().getCheckingAccountsBalance() - amount < 0) {
			throw new AccountException("withdraw: not enough balance in the sum of all checking accounts of the client",
					amount);
		}

		super.withdraw(amount);
	}

}
