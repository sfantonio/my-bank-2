package pt.ulisboa.tecnico.bank;

import java.util.HashSet;
import java.util.Set;

import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class Client {
	private final Bank bank;
	private final String firstName;
	private final String lastName;
	private int age;
	private final String NIF;
	private final String phoneNumber;
	private final String address;

	private final Set<Account> accounts = new HashSet<Account>();

	public Client(Bank bank, String firstName, String lastName, int age, String NIF, String phoneNumber, String address)
			throws ClientException, BankException, RemoteAccessException {
		checkUniqueClientNIF(bank, NIF);

		if (NIF.length() != 9 || !NIF.matches("[0-9]+")) {
			throw new ClientException("Client: NIF error", NIF);
		}

		if (phoneNumber.length() != 9 || !phoneNumber.matches("[0-9]+")) {
			throw new ClientException("Client: phoneNumber error", phoneNumber);
		}

		this.bank = bank;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.NIF = NIF;
		this.phoneNumber = phoneNumber;
		this.address = address;

		bank.addClient(this);
	}

	private void checkUniqueClientNIF(Bank bank, String NIF) throws ClientException {
		if (bank.getClientByNIF(NIF) != null) {
			throw new ClientException("createClient: duplicate client with same NIF ", NIF);
		}
	}

	public boolean isActive() {
		for (final Account account : this.accounts) {
			if (account.isActive()) {
				return true;
			}
		}
		return false;
	}

	public Bank getBank() {
		return this.bank;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public int getAge() {
		return this.age;
	}

	public String getNIF() {
		return this.NIF;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public String getAddress() {
		return this.address;
	}

	public void addAccount(Account account) {
		this.accounts.add(account);
	}

	public int getTotalBalance() {
		int result = 0;
		for (final Account account : this.accounts) {
			result = result + account.getBalance();
		}
		return result;
	}

	public boolean hasAccount(Account account) {
		return this.accounts.contains(account);
	}

	public int getNumberOfAccounts() {
		return this.accounts.size();
	}

	public void happyBirthday() throws AccountException, BankException, RemoteAccessException {
		this.age++;
		if (this.age == 18) {
			final Set<YoungAccount> youngAccounts = new HashSet<YoungAccount>();
			for (final Account account : this.accounts) {
				youngAccounts.add((YoungAccount) account);
			}
			for (final YoungAccount youngAccount : youngAccounts) {
				youngAccount.upgrade();
			}
		}
	}

	public boolean hasSalaryAccount() {
		for (final Account account : this.accounts) {
			if (account.getAccountId().startsWith(AccountType.SALARY.getPrefix())) {
				return true;
			}
		}
		return false;
	}

	public float averageBalance() {
		int numberOfActiveAccounts = 0;
		int totalBalance = 0;
		for (final Account account : this.accounts) {
			if (account.isActive()) {
				numberOfActiveAccounts++;
				totalBalance += account.getBalance();
			}
		}

		if (numberOfActiveAccounts != 0) {
			return (float) totalBalance / numberOfActiveAccounts;
		} else {
			return 0;
		}
	}

	public int getCheckingAccountsBalance() {
		int result = 0;
		for (final Account account : this.accounts) {
			if (account.getAccountId().startsWith(AccountType.CHECKING.getPrefix())) {
				result += account.getBalance();
			}
		}
		return result;
	}

}
