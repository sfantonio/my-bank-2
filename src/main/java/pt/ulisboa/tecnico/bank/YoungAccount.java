package pt.ulisboa.tecnico.bank;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class YoungAccount extends SavingsAccount {

	public YoungAccount(Client client, int balance) throws AccountException, BankException, RemoteAccessException {
		super(client, balance, 10);
	}

	@Override
	protected String getNextAccountId() {
		return AccountType.YOUNG.getPrefix() + Integer.toString(++SavingsAccount.counter);
	}

	@Override
	protected void checkCanCreateAccount(Client client) throws AccountException {
		if (client.getAge() >= 18) {
			throw new AccountException("YoungAccount: client has age greater or equal than 18 ", client.getAge());
		}
	}

	@Override
	public void withdraw(int amount) throws AccountException {
		throw new AccountException("withdraw: operation not allowed for young accounts", amount);
	}

	@Override
	public void undoWithdraw(int amount) throws AccountException {
		checkNegativeAmount(amount);
		super.undoWithdraw(amount);
	}

	public Account upgrade() throws AccountException, BankException, RemoteAccessException {
		Bank bank = getClient().getBank();
		Account checkingAccount = bank.createAccount(AccountType.CHECKING, getClient(), getBalance());
		int convertedPoints = (getPoints() / 1000) * 2;
		checkingAccount.deposit(getBalance() + convertedPoints);
		setBalance(0);
		makeInactive();
		return checkingAccount;
	}

}
