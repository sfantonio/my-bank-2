package pt.ulisboa.tecnico.bank;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class SalaryAccount extends Account {
	protected static int counter = 0;

	private int salary;

	public SalaryAccount(Client client, int balance, int salary)
			throws AccountException, BankException, RemoteAccessException {
		super(client, balance);
		this.salary = salary;
	}

	@Override
	public String getNextAccountId() {
		return AccountType.SALARY.getPrefix() + Integer.toString(++SalaryAccount.counter);
	}

	@Override
	public void checkCanCreateAccount(Client client) throws AccountException {
		if (client.hasSalaryAccount()) {
			throw new AccountException("checkCanCreateAccount: already has a salary account", 1);
		}
		super.checkCanCreateAccount(client);
	}

	@Override
	public void withdraw(int amount) throws AccountException {
		checkNegativeAmount(amount);

		if (getBalance() - amount <= -this.salary) {
			throw new AccountException("salary: negative balance below negative salary", getBalance() - amount);
		}

		super.withdraw(amount);
	}

	@Override
	public void undoWithdraw(int amount) throws AccountException {
		checkNegativeAmount(amount);

		if (getBalance() - amount <= -this.salary) {
			throw new AccountException("salary: negative balance below negative salary", getBalance() - amount);
		}

		super.undoWithdraw(amount);

	}

}
