package pt.ulisboa.tecnico.bank;

import java.util.HashSet;
import java.util.Set;

import pt.ulisboa.tecnico.bank.Account.AccountType;
import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class Bank {
	public static Set<Bank> banks = new HashSet<>();

	private final Set<Client> clients;
	private final Set<Account> accounts;
	private String bankID;

	public Bank() {
		this.accounts = new HashSet<>();
		this.clients = new HashSet<>();
		banks.add(this);
	}

	public Bank(String bankID) {
		this.accounts = new HashSet<>();
		this.clients = new HashSet<>();
		this.bankID = bankID;
		banks.add(this);
	}

	public String getBankID() {
		return bankID;
	}

	public Client createClient(String firstName, String lastName, int age, String NIF, String phoneNumber,
			String address) throws BankException, RemoteAccessException {
		Client client;
		try {
			client = new Client(this, firstName, lastName, age, NIF, phoneNumber, address);
		} catch (ClientException e) {
			throw new BankException("createClient: cannot create client", e.getValue());
		}

		return client;
	}

	public Client getClientByNIF(String NIF) {
		for (Client client : this.clients) {
			if (client.getNIF().equals(NIF)) {
				return client;
			}
		}
		return null;
	}

	public int getNumberOfClients() {
		return this.clients.size();
	}

	public Account createAccount(AccountType type, Client client, int value)
			throws BankException, AccountException, RemoteAccessException {
		Account account = createSpecificAccount(type, client, value);
		return account;
	}

	private Account createSpecificAccount(AccountType type, Client client, int value)
			throws BankException, AccountException, RemoteAccessException {
		Account account;
		switch (type) {
		case CHECKING:
			account = new CheckingAccount(client);
			break;
		case SAVINGS:
			account = new SavingsAccount(client, 0, value);
			break;
		case SALARY:
			account = new SalaryAccount(client, 0, value);
			break;
		case YOUNG:
			account = new YoungAccount(client, 0);
			break;
		case SERVICE:
			account = new ServiceAccount(client);
			break;
		default:
			throw new BankException("createAccount: undefined account type",
					client.getFirstName() + " " + client.getLastName());
		}
		return account;
	}

	public void makeAccountInactive(Account account) throws BankException, AccountException, RemoteAccessException {
		account.makeInactive();
	}

	public int getNumberOfAccounts() {
		return this.accounts.size();
	}

	public int getNumberOfActiveAccountsAccounts() {
		int result = 0;
		for (Account account : this.accounts) {
			if (account.isActive()) {
				result++;
			}
		}
		return result;
	}

	public void addAccount(Account account) throws BankException, RemoteAccessException {
		if (account.getBank() != this) {
			throw new BankException("addAccount: the account does not belong to this bank", account.getAccountId());
		}
		this.accounts.add(account);
	}

	public void addClient(Client client) throws BankException, RemoteAccessException {
		if (client.getBank() != this) {
			throw new BankException("addClient: the client does not belong to this bank", client.getNIF());
		}
		this.clients.add(client);
	}

	public int getTotalBalance() {
		int result = 0;
		for (Account account : this.accounts) {
			result += account.getBalance();
		}
		return result;
	}

	public Account getAccountById(String accountId) {
		for (Account account : this.accounts) {
			if (account.getAccountId().equals(accountId)) {
				return account;
			}
		}
		return null;
	}

	public Set<Account> getAccountSet() {
		return this.accounts;
	}

}
