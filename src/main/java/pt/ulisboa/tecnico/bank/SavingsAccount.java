package pt.ulisboa.tecnico.bank;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class SavingsAccount extends Account {
	protected static int counter = 0;

	@Override
	protected String getNextAccountId() {
		return AccountType.SAVINGS.getPrefix() + Integer.toString(++SavingsAccount.counter);
	}

	private final int base;
	private int points = 0;

	public SavingsAccount(Client client, int balance, int multiple)
			throws AccountException, BankException, RemoteAccessException {
		super(client, balance);
		this.base = multiple;
	}

	@Override
	public void deposit(int amount) throws AccountException {
		if (amount % this.base != 0) {
			throw new AccountException("deposit: it is not a multiple", amount);
		}

		this.points = getPoints() + amount / this.base;

		super.deposit(amount);
	}

	@Override
	public void undoDeposit(int amount) throws AccountException {
		this.points = getPoints() + amount / this.base;

		super.deposit(amount);
	}

	@Override
	public void withdraw(int amount) throws AccountException {
		checkNegativeAmount(amount);

		if (amount != getBalance()) {
			throw new AccountException("withdraw: should be equal to balance", amount);
		}

		super.withdraw(amount);
	}

	@Override
	public void undoWithdraw(int amount) throws AccountException {
		checkNegativeAmount(amount);
		this.points = getPoints() - amount / this.base;
		super.undoWithdraw(amount);
	}

	public int getBase() {
		return this.base;
	}

	public int getPoints() {
		return this.points;
	}

}
