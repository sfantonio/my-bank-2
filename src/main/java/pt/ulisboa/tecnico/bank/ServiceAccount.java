package pt.ulisboa.tecnico.bank;

import pt.ulisboa.tecnico.bank.exception.AccountException;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;

public class ServiceAccount extends Account {
	protected static int counter = 0;

	public ServiceAccount(Client client) throws AccountException, BankException, RemoteAccessException {
		super(client);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getNextAccountId() {

		return AccountType.SERVICE.getPrefix() + Integer.toString(++ServiceAccount.counter);

	}

	@Override
	public void withdraw(int amount) throws AccountException {
		throw new AccountException("Cannot withdraw from service account", 0);

	}

	@Override
	public void undoWithdraw(int amount) throws AccountException {
		checkNegativeAmount(amount);
		super.undoWithdraw(amount);
	}

}
