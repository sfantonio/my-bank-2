package pt.ulisboa.tecnico;

import pt.ulisboa.tecnico.bank.Bank;
import pt.ulisboa.tecnico.bank.CheckingAccount;
import pt.ulisboa.tecnico.bank.Client;
import pt.ulisboa.tecnico.bank.exception.BankException;
import pt.ulisboa.tecnico.bank.exception.ClientException;
import pt.ulisboa.tecnico.bank.exception.RemoteAccessException;
import pt.ulisboa.tecnico.presentation.BankItem;
import pt.ulisboa.tecnico.presentation.CancelDirectDebitItem;
import pt.ulisboa.tecnico.presentation.ChangeOperationDoneToUndoItem;
import pt.ulisboa.tecnico.presentation.ChangeOperationUndonetoDoItem;
import pt.ulisboa.tecnico.presentation.ChangeOperationsStateItem;
import pt.ulisboa.tecnico.presentation.ClientItem;
import pt.ulisboa.tecnico.presentation.CompositeMenu;
import pt.ulisboa.tecnico.presentation.CreateAccountItem;
import pt.ulisboa.tecnico.presentation.DepositItem;
import pt.ulisboa.tecnico.presentation.Menu;
import pt.ulisboa.tecnico.presentation.ProcessOperationsItem;
import pt.ulisboa.tecnico.presentation.RegisterAuthorizationItem;
import pt.ulisboa.tecnico.presentation.RequestDirectDebitItem;
import pt.ulisboa.tecnico.presentation.SubmitPaymentExpectationItem;
import pt.ulisboa.tecnico.presentation.SubmitPaymentItem;
import pt.ulisboa.tecnico.presentation.SubmitTransferItem;
import pt.ulisboa.tecnico.presentation.WithdrawItem;

public class Application {
	private static Application instance;

	public static Application getInstance() {
		return instance;
	}

	private Menu menu;
	private Menu context;

	private Application() throws ClientException, BankException, pt.ulisboa.tecnico.bank.exception.AccountException,
			RemoteAccessException {
		initializeDatabase();
		initializeMenu();
		this.context = this.menu;
	}

	private void initializeDatabase() throws ClientException, BankException,
			pt.ulisboa.tecnico.bank.exception.AccountException, RemoteAccessException {
		Bank bank = new Bank("Santander");
		Bank bank2 = new Bank("BPI");
		Client client = new Client(bank, "José", "Manuel", 45, "123456789", "987654321", "Beco");
		Client client2 = new Client(bank2, "José", "Manuel", 45, "987654321", "987654521", "Beco");

		new CheckingAccount(client);
		new CheckingAccount(client2);

	}

	private void initializeMenu() {
		CompositeMenu topMenu = new CompositeMenu("Money make the world around!");
		CompositeMenu sibsMenu = new CompositeMenu("SIBS Operations");
		CompositeMenu bankMenu = new CompositeMenu("Bank Operations");

		topMenu.addMenu(sibsMenu);
		topMenu.addMenu(bankMenu);

		bankMenu.addMenu(new DepositItem());

		bankMenu.addMenu(new WithdrawItem());

		bankMenu.addMenu(new ClientItem());

		bankMenu.addMenu(new BankItem());

		bankMenu.addMenu(new CreateAccountItem());

		sibsMenu.addMenu(new SubmitTransferItem());

		sibsMenu.addMenu(new SubmitPaymentExpectationItem());

		sibsMenu.addMenu(new SubmitPaymentItem());

		sibsMenu.addMenu(new ProcessOperationsItem());

		sibsMenu.addMenu(new RegisterAuthorizationItem());

		sibsMenu.addMenu(new RequestDirectDebitItem());

		sibsMenu.addMenu(new CancelDirectDebitItem());

		sibsMenu.addMenu(new ChangeOperationsStateItem());

		sibsMenu.addMenu(new ChangeOperationDoneToUndoItem());

		sibsMenu.addMenu(new ChangeOperationUndonetoDoItem());

		this.menu = topMenu;
	}

	public void setContext(int option) {
		if (option == 0) {
			Menu newContext = this.menu.getParentItem(this.context);
			if (newContext != null) {
				this.context = newContext;
			}
		} else {
			this.context = this.context.getItem(option);
		}
	}

	private void run() {
		while (true) {
			try {
				this.context.execute();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public static void main(String[] args) throws ClientException, BankException,
			pt.ulisboa.tecnico.bank.exception.AccountException, RemoteAccessException {
		instance = new Application();
		instance.run();
	}
}
